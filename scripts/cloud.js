

/**
*  Inserts the information into a spreasheet 
*  in date - the date the exercise was performed
*  in day  - the dat of the week ( 1 - 7 ) 
*  in name - the name of the exercise 
*  in set  - the set # performed 
*  in rep  - the rep count performed
*  in weight - the amount of weight lifted
* out - true or false depending if the write was successful or not.
*/
function testCase(){

  
 var filesheet = getSheet(1);
 var sheet = filesheet.getActiveSheet();
 
 insertRowAt(sheet, 5);

//  var date   = "2015/10/9";
//  var day    = 5;
//  var name   = "ABs";
//  var weight = 80;
//  var reps   = 9
//  
//  insertDataPoint(date,day,name,weight,reps);
}


function generateRoutineSheet(){

  var root = DriveApp.getRootFolder();
  var folders = root.getFolders();
  var folderSet = {};
  var folderExists = false;  
  var ss;  
  var folder
  
  
  //Check if our folder exists, if not make one 
  while (folders.hasNext()) {
    var tempfolder = folders.next();
    if(tempfolder.getName() == "Routines"){      
        folderExists = true;
        folder = tempfolder;
        break;
    }  
  }  
  if(!folderExists){
     Logger.log("Creating a new folder");
     folder = root.createFolder("Routines")     
  }
  
  //Check if our files exists, if yes, destroy it
  var files = DriveApp.getFoldersByName("Routines").next().getFiles();
  while(files.hasNext()){
    var file = files.next();
    if(file.getName() == "My Routine"){
        folder.removeFile(file)      
        break;   
    }
  }

  ss =  createSpreadsheet("My Routine",folder);
  generateEmptySkeletonSheet(ss)        
  return "Successfull";

}

function generateEmptySkeletonSheet(filesheet){
 var cell; 
 var sheet = filesheet.getActiveSheet();
 var instructionBgColour = "yellow";
 
 //Add instructions
 setInstructionLine(sheet, 1, "To import successfully, follow these guidelines");
 setInstructionLine(sheet, 2, "1. Exercise entries will be read in top to bottom and left to right fashions. Every entry under the weekday labels will count as an exercise for that day");
 setInstructionLine(sheet, 3, "2. Exercise names cannot be the same as a weekday label.");
 setInstructionLine(sheet, 4, "3. Every exercise entry needs to have its information laid out in proper column order as indicated by the grey cell rows."); 
 setInstructionLine(sheet, 5, "4. The required fields are: Name, Sets, Mininum Reps, Maximum Reps, and Weight. If any of these fields are left blank, the row will be skipped");
 setInstructionLine(sheet, 6, "5. You are free to add as many exercises as you want. To move entire sections without having to copy, right click on the row index (far left) and select Insert 1 above or below.");
 setInstructionLine(sheet, 7, "6. Cells with red font and yellow backgrounds can only be moved up or down. Do not modify the value. _END must be under everything!");
//explain the columns 
 setInstructionLine(sheet, 9, "Column names and their meanings");
 setColumnInstruction(sheet, 10, "Sets","the number of sets to perform for the exercise");
 setColumnInstruction(sheet, 11, "Minimum Reps" ,"the minimum rep to perform for every set");
 setColumnInstruction(sheet, 12, "Maximum Reps" ,"the maximum rep to perform for every set");
 setColumnInstruction(sheet, 13, "Weight" ,"the amount of weight to lift for the exercise");
 setColumnInstruction(sheet, 14, "Is Weight Incrementing?","Indicates whether the weight to lift should be incremented once per week"); 
 setColumnInstruction(sheet, 15, "Weight Increment Amount","The amount to increment the weight value");
 setColumnInstruction(sheet, 16, "Is Rep Incrementing?","Indicates whether the rep value should be increment once per week");
 setColumnInstruction(sheet, 17, "Rep Increment Amount","The amount to increment the rep value ");
 setColumnInstruction(sheet, 18, "Note ","A note or tip about the exercise.");
  
//explain behaviours
 setInstructionLine(sheet, 20, "Behaviour of Various Settings");
 setBehaviourInstruction(sheet, 21, "If IsIncrementWeight and IsIncrementRep are both false: ", "No changes will be made to the exercises at any point");
 setBehaviourInstruction(sheet, 22, "If IsIncrementWeight is true and IsIncrementingRep is false:", "The weight value will increment once every week by the value indicated by Weight Increment Amount");
 setBehaviourInstruction(sheet, 23, "If IsIncrementWeight is false and IsIncrementingRep is true:","The rep value will increment once every week by the value indicated by Rep Increment Amount");
 setBehaviourInstruction(sheet, 24, "If IsIncrementWeight and IsIncrementRep are both true:","The rep value will be increment by Rep Increment value once every week until reaching Maximum Rep Value.");
 setBehaviourInstruction(sheet, 25, "", "Then, rep value will change to Mininum Rep value, but the Weight value will be incremented by Weight Increment Amount.");
 
//Add sunday
 createRowEntry(sheet,true,27,"Sunday","Sets","Mininum Reps","Maximum Reps","Weight","Is Weight Incrementing?","Weight Increment Amount","Is Rep Incrementing?","Rep Increment Amount","Note");
 createRowEntry(sheet,false,28,"Exercise 1","3","8","8","30","false","2.5","false","1","Its all about the posture"); 
 createRowEntry(sheet,false,29,"Exercise 2","3","8","8","30","false","2.5","false","1","Its all about the posture");
 createRowEntry(sheet,false,30,"Etc..","3","8","8","30","false","2.5","false","1","Its all about the posture");
 // add monday
 createRowEntry(sheet,true,32,"Monday","Sets","Mininum Reps","Maximum Reps","Weight","Is Weight Incrementing?","Weight Increment Amount","Is Rep Incrementing?","Rep Increment Amount","Note");
 createRowEntry(sheet,false,33,"Hypertrophy Exercise 1","3","8","8","30","false","2.5","false","1","Its all about the posture"); 
 createRowEntry(sheet,false,34,"Hypertrophy Exercise 2","3","8","8","30","false","2.5","false","1","Its all about the posture");
 createRowEntry(sheet,false,35,"Etc.. ","3","8","8","30","false","2.5","false","1","Its all about the posture");
 
 //add more weekdays
 createRowEntry(sheet,true,37,"Tuesday","Sets","Mininum Reps","Maximum Reps","Weight","Is Weight Incrementing?","Weight Increment Amount","Is Rep Incrementing?","Rep Increment Amount","Note");
 createRowEntry(sheet,true,39,"Wednesday","Sets","Mininum Reps","Maximum Reps","Weight","Is Weight Incrementing?","Weight Increment Amount","Is Rep Incrementing?","Rep Increment Amount","Note");
 createRowEntry(sheet,true,41,"Thursday","Sets","Mininum Reps","Maximum Reps","Weight","Is Weight Incrementing?","Weight Increment Amount","Is Rep Incrementing?","Rep Increment Amount","Note");
 createRowEntry(sheet,true,43,"Friday","Sets","Mininum Reps","Maximum Reps","Weight","Is Weight Incrementing?","Weight Increment Amount","Is Rep Incrementing?","Rep Increment Amount","Note");
 createRowEntry(sheet,true,45,"Saturday","Sets","Mininum Reps","Maximum Reps","Weight","Is Weight Incrementing?","Weight Increment Amount","Is Rep Incrementing?","Rep Increment Amount","Note");

 addStopperLine(sheet,47);
}

function setInstructionLine(sheet, rowIndex, instruction){
 var cell;
 var instructionBgColour = "#DDDDDD";
 cell = sheet.getRange(rowIndex, 1);
 cell.setValue(instruction);
 cell = sheet.getRange(rowIndex, 1,1,10);
 cell.setBackground(instructionBgColour);
}

function setColumnInstruction(sheet, rowIndex, columnname, description){
 var cell;
 var instructionBgColour = "#DDDDDD";
 cell = sheet.getRange(rowIndex, 1);
 cell.setValue(columnname);
 cell = sheet.getRange(rowIndex, 3);
 cell.setValue(description); 
 cell = sheet.getRange(rowIndex, 1,1,10);
 cell.setBackground(instructionBgColour);
}

function setBehaviourInstruction(sheet, rowIndex, columnname, description){
 var cell;
 var instructionBgColour = "#DDDDDD";
 cell = sheet.getRange(rowIndex, 1);
 cell.setValue(columnname);
 cell = sheet.getRange(rowIndex, 5);
 cell.setValue(description); 
 cell = sheet.getRange(rowIndex, 1,1,10);
 cell.setBackground(instructionBgColour);
}

function addStopperLine(sheet,rowIndex){
  var cell;
  cell =sheet.getRange(rowIndex,1);
  cell.setValue("_END");
  cell.setBackground("yellow");
  cell.setFontColor("red");

}


function createRowEntry(sheet,isLabel,rowIndex, 
name, sets,minRep,maxRep,weight,isIncWeight,weightIncAmount,
isRepInc,repIncAmount,note){
  var cell;
  
  cell = sheet.getRange(rowIndex, 1);
  cell.setValue(name);
  cell.setBackground(isLabel ? "grey":"white");
  cell = sheet.getRange(rowIndex, 2);
  cell.setValue(sets);
  cell.setBackground(isLabel ? "grey":"white");
  cell = sheet.getRange(rowIndex, 3);
  cell.setValue(minRep);
  cell.setBackground(isLabel ? "grey":"white");
  cell = sheet.getRange(rowIndex, 4);
  cell.setValue(maxRep);
  cell.setBackground(isLabel ? "grey":"white");
  cell = sheet.getRange(rowIndex, 5);
  cell.setValue(weight);
  cell.setBackground(isLabel ? "grey":"white");
  cell = sheet.getRange(rowIndex, 6);
  cell.setValue(isIncWeight);
  cell.setBackground(isLabel ? "grey":"white");
  cell = sheet.getRange(rowIndex, 7);
  cell.setValue(weightIncAmount);
  cell.setBackground(isLabel ? "grey":"white");
  cell = sheet.getRange(rowIndex, 8);
  cell.setValue(isRepInc);
  cell.setBackground(isLabel ? "grey":"white");
  cell = sheet.getRange(rowIndex, 9);
  cell.setValue(repIncAmount);
  cell.setBackground(isLabel ? "grey":"white");
  cell = sheet.getRange(rowIndex, 10);
  cell.setValue(note);
  cell.setBackground(isLabel ? "grey":"white");
}


function getExerciseImport(){

  var filesheet = findImportSheet("My Routine");
  
  if(filesheet != "unsuccessful"){
      var sheet = filesheet.getActiveSheet();
      var table = {};
      table["Monday"] = [];
      table["Tuesday"] = [];
      table["Wednesday"] = [];
      table["Thursday"] = [];
      table["Friday"] = [];
      table["Saturday"] = [];
      table["Sunday"] = [];
      //loop through first column, 
      
      var rowIndex = 1;
      var column1Value = sheet.getRange(rowIndex,1).getValue();   
      var lastWeek = column1Value;
      Logger.log(lastWeek)
           
      while(column1Value != "_END"){ 
               
        if(column1Value == "Saturday") { lastWeek = "Saturday";  Logger.log(lastWeek);} 
        else if (column1Value == "Sunday") {lastWeek = "Sunday";Logger.log(lastWeek); }
        else if (column1Value == "Monday") {lastWeek = "Monday"; Logger.log(lastWeek);}
        else if (column1Value == "Tuesday") {lastWeek = "Tuesday"; Logger.log(lastWeek);}
        else if (column1Value == "Wednesday") {lastWeek = "Wednesday";Logger.log(lastWeek); }
        else if (column1Value == "Thursday") {lastWeek = "Thursday"; Logger.log(lastWeek);}
        else if (column1Value == "Friday") {lastWeek = "Friday"; Logger.log(lastWeek);}
        else if (column1Value != "" ){
                   
           var row = sheet.getRange(rowIndex, 1, 1,11).getValues()[0];
           
           if(row[0] != "" && row[1] != "" && row[2] != "" && row[3] != "" && row[4] != ""){
            var exercise = {}  
           
           //Logger.log(row);
           exercise.name = row[0];
           exercise.sets = row[1];
           exercise.repsMin = row[2];
           exercise.repsMax = row[3];
           exercise.weight  = row[4];
           exercise.isWeightIncrement = row[5];         
           exercise.weightIncrement = row[6];
           exercise.isRepIncrement =  row[7]; 
           exercise.repIncrement = row[8];
           exercise.note =  row[9];
           exercise.type = "Standard";
           exercise.id  = table[lastWeek].length;
           exercise.dayOfWeek = getDayInt(lastWeek);
           exercise.attemps = 0;   
           
           //todo iterator through remaining blocks
           //todo validate the entries 
           //check 
           if(row[10].substring(0,2) == "MR"){             
               exercise.attribute0 = 1;
               exercise.value0     = row[10].substring(3)
           }          
           
           table[lastWeek].push(exercise);   
           }                                                
        }   
        rowIndex++;
        column1Value = sheet.getRange(rowIndex,1).getValue();
      }     
//      Logger.log(table)
      return JSON.stringify(table);
  }
  else {
   Logger.log("Cannot find file or folder")
   return "unsuccessful"
  }
  
}


function findImportSheet(filename){  
   //Check if our files exists, if not make one
   var files = DriveApp.getFoldersByName("Routines").next().getFiles();
   if(files == null){
      return "unsuccessful"
   }

   while(files.hasNext()){
   var file = files.next();
   if(file.getName() == filename){
        return SpreadsheetApp.open(file)
        break;   
    }
   }
   return "unsuccessful" 
}


function insertDataPoint(date, day, name, weight, reps){
  
  var filesheet = getSheet(day);
  var sheet = filesheet.getActiveSheet();
 
  var startRowIndex = getStartingPoint(sheet);      //indicates the position of _begin
  var endingRowIndex;                                    //indicates the position of _end
  var startCell = getCellValue(sheet,startRowIndex,1);   //holds the value of the starting block

  if(startCell == ""){ 
    Logger.log("Cell does not exist"); 
    startBlock(sheet,startRowIndex, date);  
    startCell = getCellValue(sheet,startRowIndex,1);     
  }
   
  if (startCell == "_begin"){
    Logger.log("Cell exists");
    //find the _end 
           
   //does this date match the incoming? 
   if(isCorrectBlock(sheet, startRowIndex, date)){
       Logger.log("Date Matches");
       
       //loop until name matches or until you hit _end
      var index = startRowIndex + 2;
      var foundExerciseMatch = false;
      while(getCellValue(sheet,index ,1)  != "_end"){
      
        if(getCellValue(sheet,index ,1) == name){
          foundExerciseMatch = true;
          break;
        }                                   
        index = index + 1;
      }    
      
      if(!foundExerciseMatch){
         //write the new exercise to this column
         Logger.log("Reached end, but did not find match, adding to list");         
         createExerciseEntry(sheet, index, name);   
      }
               
      //write the entry entry
      addEntry(sheet, index, weight,reps);
   
   }else{
     Logger.log("Date does not match");
     //this is a new date
     //indicate that we are starting a new block
     incrementMeta(sheet, 3 + getCellValue(sheet, 1, 3))
     startRowIndex = getStartingPoint(sheet);
     startBlock(sheet,startRowIndex, date);  
  
  ///Put this into a function
      var index = startRowIndex + 2;
      var foundExerciseMatch = false;
      while(getCellValue(sheet,index ,1)  != "_end"){
      
        if(getCellValue(sheet,index ,1) == name){
          foundExerciseMatch = true;
          break;
        }                                   
        index = index + 1;
      }    
      
      if(!foundExerciseMatch){
         //write the new exercise to this column
         Logger.log("Reached end, but did not find match, adding to list");         
         createExerciseEntry(sheet, index, name);   
      }
               
      //write the entry entry
      addEntry(sheet, index, weight,reps);   
   }
  }         
  SpreadsheetApp.flush();
  
  return "successful"
 
}






/**
 * Return the set of folder names contained in the user's root folder as an
 * object (with folder IDs as keys).
 * @return {Object} A set of folder names keyed by folder ID.
 */
function getSheet(day) {
  var root = DriveApp.getRootFolder();
  var folders = root.getFolders();
  var folderSet = {};
  var folderExists = false;
  
  var ss;
  
  var folder
  //Check if our folder exists, if not make one 
  while (folders.hasNext()) {
    var tempfolder = folders.next();
    if(tempfolder.getName() == "Routines"){      
        folderExists = true;
        folder = tempfolder;
        break;
    }  
  }  
  if(!folderExists){
     Logger.log("Creating a new folder");
     folder = root.createFolder("Routines")     
  }
  
  //Check if our files exists, if not make one
  var files = DriveApp.getFoldersByName("Routines").next().getFiles();
  var fileExists = false;
  while(files.hasNext()){
   var file = files.next();
   if(file.getName() == getSheetTitle(day)){
      fileExists = true;
        break;   
   }
  }
  
  if(!fileExists){
     Logger.log("Creating a new spreadsheet");
     ss =  createSpreadsheet(getSheetTitle(day),folder);
     startSheet(ss)
         
  }else{
     ss = getFile(day); 
     Logger.log("Fetching existing file");
  }
    
  return ss;
}

/**
*  Returns a weekday text 
*  in day - day of week (1 - 7) 
*  out day - returns day as text
*/
function getSheetTitle(day){
  if      (day == 1){return "Sunday"  ;} 
  else if (day == 2){return "Monday"   ; }
  else if (day == 3){return "Tuesday"  ; }
  else if (day == 4){return "Wednesday"; }
  else if (day == 5){return "Thursday" ; }
  else if (day == 6){return "Friday"   ; }
  else if (day == 7){return "Saturday" ; }
}
/**
*  Returns a weekday text 
*  in dat - day of week (1 - 7)
*  out day - returns one value from 1 (sunday) through 7 (saturday)
*/ 
function getDayInt(day){
if(day == "Sunday") {return 1;}
if(day == "Monday") {return 2;}
if(day == "Tuesday") {return 3;}
if(day == "Wednesday") {return 4;}
if(day == "Thursday") {return 5;}
if(day == "Friday") {return 6;}
if(day == "Saturday") {return 7;}

}



/**
*  Creates a new spreadsheet file on the user's google drive 
* in name - the name of the spreadsheet
* int parent - the folder to create the file in 
* out ss - a spreadsheet object
*/
function createSpreadsheet( name, parent ){
  this.ssTmp = SpreadsheetApp.create( name );
  this.idTmp = this.ssTmp.getId();
  this.fileTmp = DriveApp.getFileById( this.idTmp );
  if ( parent ) {
    // this.file = parent.createFile( name, '', MimeType.GOOGLE_SHEETS );
    this.file = this.fileTmp.makeCopy( name, parent );
    DriveApp.getRootFolder().removeFile( this.fileTmp );
  } else {
    // this.file = DriveApp.createFile( name, '', MimeType.GOOGLE_SHEETS );
    this.file = this.fileTmp.makeCopy( name, folderReports );
    DriveApp.getRootFolder().removeFile( this.fileTmp );
  }
  this.id = this.file.getId();
  this.ss = SpreadsheetApp.openById( this.id );
  return ss;
}

/**
* fetches a spreadsheet based on the given day
* in day (int ) - the day of the week (1-7)
*/
function getFile(day){
  this.id = DriveApp.getFoldersByName("Routines").next().getFilesByName(getSheetTitle(day)).next();
  this.ss = SpreadsheetApp.open(id);
  return ss;
}

/**
* Prepares the sheet for writing exercises to 
* in filesheet  - the spreadsheet to work on 
*/
function startSheet(filesheet){
 
 var fontColour = "yellow";
 var cell; 
 var sheet = filesheet.getActiveSheet();
 
 cell = sheet.getRange(1, 1);
 cell.setValue("_meta");
 cell.setFontColor(fontColour);
 
 cell = sheet.getRange(1, 2);
 cell.setValue(3);
 cell.setFontColor(fontColour);
 
 cell = sheet.getRange(1, 3);
 cell.setValue(0);
 cell.setFontColor(fontColour);
 
 cell = sheet.getRange(1, 4);
 cell.setValue("Do not Modify this row");
 cell.setFontColor("red");
 
 cell = sheet.getRange(1,1,1,26);
 cell.setBackground("yellow");
 
// sheet.getRange(1, 1).setValue("_meta");
// sheet.getRange(1, 2).setValue(3);
// sheet.getRange(1, 3).setValue(0);
// sheet.getRange(1, 4).setValue("!!Do not Modify this row!!")
}




/** 
* Returns the starting index of our exercise block
* in sheet - the sheet we will be working on 
*/
function getStartingPoint(sheet){
 return getCellValue(sheet,1,2);


}

/**
* Returns the value at the indicate cell coordinates 
* in sheet - the active sheet
* in rowIndex - the row index of the sheet
*  in columnIndex = the column index of the sheet
* out  - the value of the cell
*/
function getCellValue(sheet, rowIndex, columnIndex){
  var dataRange = sheet.getRange(rowIndex, columnIndex, 1, 1);
  var data = dataRange.getValues();
  var row = data[0];  
  var col1 = row[0];
  Logger.log("Cell value: " + col1);
  return col1
}

/**
* Creates a new block for the current day 
*  in sheet - the current active sheet to eidt
*  in rowIndex - the row index to start modifying to 
* in date  - the date of the block
*/
function startBlock(sheet, rowIndex,date){

   var cell; 
   cell =  sheet.getRange(rowIndex ,    1);
   cell.setValue("_begin"); 
   cell.setFontColor("white");
   
   cell = sheet.getRange(rowIndex ,    1,1,26);
   cell.setBorder(false, false, true, false,false, false)
   
   cell =  sheet.getRange(rowIndex + 1, 1);
   cell.setValue(date);
   cell.setFontColor("black");
   
   cell =  sheet.getRange(rowIndex + 1, 2);
   cell.setValue("Volume");
   cell = sheet.getRange(rowIndex + 1 ,3);
   cell.setValue("Sets(WeightxReps)");
   
   cell = sheet.getRange(rowIndex +1 ,    1,1,26);
   cell.setBackground("#CCCCCC");
     
   cell =  sheet.getRange(rowIndex + 2 ,1);
   cell.setValue("_end");
   cell.setFontColor("white");
   
   cell = sheet.getRange(rowIndex +2 ,    1,1,26);
   cell.setBorder(true, false, false, false,false, false)
   
//   sheet.getRange(rowIndex ,    1).setValue("_begin");
//   sheet.getRange(rowIndex + 2 ,1).setValue("_end")   
     
   resetExerciseCount(sheet);
}

/**
* Checks if the current block of data is valid for editing 
* in sheet - the current active sheet to edit
* in rowIndex - the row index to start modifying to 
* in date  - the incoming date we will use to check if block is correct
*/
function isCorrectBlock(sheet, blockIndex, incomingDate ){

   //write to the cell to autoconvert format 
   sheet.getRange(blockIndex + 1, 2).setValue(incomingDate)
   //get the integer date  
   var incoming =  sheet.getRange(blockIndex + 1, 2).getValue().valueOf();
   //get the value we are comparing with
   var cellValue = getCellValue(sheet,blockIndex + 1 ,1).valueOf();     
   //Logger.log("Found : "+ date + " have " +  incoming)  
   sheet.getRange(blockIndex + 1, 2).setValue("Volume");
   return cellValue == incoming;
}

/**
* inserts a new row exercise entry 
* in sheet - the current active sheet to edit
* in rowIndex - the row index to start modifying 
* in name - the name of the exercise 
*/
function createExerciseEntry(sheet, rowIndex, name){
 
 var cell;

  cell = sheet.getRange(rowIndex , 1);
  cell.setValue(name);
  cell.setFontColor("black");
  cell.setBackground("grey");
  cell = sheet.getRange(rowIndex , 1,1,26);
  cell.setBorder(false, false, false, false,false, false)
  
  cell = sheet.getRange(rowIndex + 1, 1);
  cell.setValue("_end");
  cell.setFontColor("white");  
  cell = sheet.getRange(rowIndex +1 ,    1,1,26);
  cell.setBorder(true, false, false, false,false, false) 
  sheet.getRange(1,3).setValue(sheet.getRange(1,3).getValue() + 1);
  
}

/** writes a data point in the next available slot *
* in sheet - the active spreadsheet to edit to
* in row   - the row index
* in data  - the data value to edit
*/
function addEntry(sheet, row, weight, reps){ 

  var volume = getCellValue(sheet,row ,2) ; 
  var volumeCell =  sheet.getRange(row , 2);
  volumeCell.setValue(getCellValue(sheet,row ,2)  + (weight * reps ));
  volumeCell.setBackground("#DDDDDD");
  volumeCell.setBorder(false,false,true,true,false,false);
  
   var columnIndex =  3
   while(getCellValue(sheet,row ,columnIndex)  != ""){                                           
        columnIndex = columnIndex + 1;
  }  
  sheet.getRange(row , columnIndex).setValue("" + weight + "x"+reps);
}



function incrementMeta(sheet, incrementValue){
  sheet.getRange(1 , 2).setValue(getStartingPoint(sheet) + incrementValue);  
}

function incrementExerciseCount(sheet, incrementValue){
  sheet.getRange(1 , 3).setValue(getCellValue(sheet, 1, 3) + incrementValue);  
}
function resetExerciseCount(sheet){
 sheet.getRange(1 , 3).setValue(0);  
}

function insertRowAt(sheet, insertRowIndex){

  //get the rowIndex of the last line in the file 
  var startRowIndex = getStartingPoint(sheet);
  var lastRowIndex = startRowIndex + 2;
  while(getCellValue(sheet,lastRowIndex ,1)  != ""){
     lastRowIndex = lastRowIndex + 1;
  }    

  Logger.log("final _end is at " + lastRowIndex);
      
  //copy entire block   
   var sourceRange = sheet.getRange(insertRowIndex, 1,lastRowIndex-insertRowIndex ,26);
   var destinationRange = sheet.getRange(insertRowIndex + 1, 1, lastRowIndex-insertRowIndex, 26);
   
   sourceRange.copyTo(destinationRange);
   
 sheet.getRange(insertRowIndex, 1, 1, 26).setValue("");
   incrementMeta(sheet, 1);
   
}




function synchronizeData(collection){
  
  var filesheet;
  var sheet;
  var referencedDay; 
  
  var obj = JSON.parse(collection);
  var points = obj.points;
  
  //for each point
  for(var i = 0; i < points.length; i++){
  
    var item = points[i];
  
    var date   = item.date;
    var day    = item.day;
    var name   = item.name;
    var weight = item.weight;
    var reps   = item.reps;
    
    //get the sheet to edit for this data point, 
    if(day != referencedDay){
      filesheet = getSheet(day);
      sheet = filesheet.getActiveSheet();
    }
    referencedDay = day;
    
    //we start by checking the the latest block
    var startRowIndex = getStartingPoint(sheet);
    
//    sheet.getRange(2,3).setValue("StartRowIndex");
//    sheet.getRange(2,4).setValue(startRowIndex);
           
//    sheet.getRange(3,3).setValue("Match at MetaIndex?");
    if(isCorrectBlock(sheet, startRowIndex, date)){    
//         sheet.getRange(3,4).setValue("Yes");
         insertDataPoint(date,day,name,weight,reps);
    }else{
    
//      sheet.getRange(3,4).setValue("no");
    
      //latest block date did not match, loop through previous blocks      
      var dataRange = sheet.getRange(1, 1,startRowIndex,1);
      var data = dataRange.getValues();
      var value;
      
      var isDataInserted = false;
      
      for(var j=startRowIndex -1; j >=0 ; j--){
         var row = data[j];
         var value = row[0];
//         
         if(value == "_begin"){
         
//          sheet.getRange(2,7).setValue("Begin at");
//          sheet.getRange(2,8).setValue(j+1);
            if(isCorrectBlock(sheet, j + 1, date)){
            
//                sheet.getRange(2,5).setValue("MatchIndex:");
//                sheet.getRange(2,6).setValue(j + 1);
                                  
              //loop until name matches or until you hit _end
              var index = j + 2;
              var foundExerciseMatch = false;
              while(getCellValue(sheet,index ,1)  != "_end"){
      
                if(getCellValue(sheet,index ,1) == name){
                  foundExerciseMatch = true;
                  break;
                }                                   
                index = index + 1;
              }    
           
              //did not find a matching exercise name, insert a new row
              if(!foundExerciseMatch){
//                 //write the new exercise to this column
//                 Logger.log("Reached end, but did not find match, adding to list");         
//                 createExerciseEntry(sheet, index, name);   
                   insertRowAt(sheet, index ); 
                   var cell = sheet.getRange(index,1);
                   cell.setValue(name);
                   cell.setFontColor("black");
                   cell.setBackground("grey");
                                                      
              }
              addEntry(sheet, index, weight,reps);   
              isDataInserted = true;
//              //write the entry entry
              break;
              
            }  //if date matches       
         }  //if _being block     
      } //for 
      
      
      if(isDataInserted == false){
        //at this point we didn't find it, so insert the value normally
        insertDataPoint(date,day,name,weight,reps);         
      }      
    }                 
  }  
}





