package com.gorillamoa.routines.interfaces;


/**
 * Created by alvaregd on 15/11/15.
 */
public interface NetworkCallbacks {

  void AuthException(Exception error);
  void OnSuccess();

}
