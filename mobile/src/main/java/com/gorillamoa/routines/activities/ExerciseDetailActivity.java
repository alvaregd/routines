package com.gorillamoa.routines.activities;

import android.accounts.AccountManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.script.model.ExecutionRequest;
import com.google.api.services.script.model.Operation;
import com.gorillamoa.routines.R;
import com.gorillamoa.routines.app.App;
import com.gorillamoa.routines.data.DetailRequestParams;
import com.gorillamoa.routines.data.Exercise;
import com.gorillamoa.routines.data.ExerciseCollection;
import com.gorillamoa.routines.data.ExerciseGroup;
import com.gorillamoa.routines.fragments.ExerciseDetailFragment;
import com.gorillamoa.routines.fragments.ExerciseDetailFragmentEdit;
import com.gorillamoa.routines.network.NetworkUtils;
import com.gorillamoa.routines.utils.CryptoUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * An activity representing a single Exercise detail screen. This
 * activity is only used narrow width devices. On tablet-size devices,
 * item details are presented side-by-side with a list of items
 * in a {@link ExerciseListActivity}.
 */
public class ExerciseDetailActivity extends AppCompatActivity implements
        ExerciseDetailFragmentEdit.EditListener,
        ExerciseDetailFragment.NetworkRequestListener{
    private static final String TAG = ExerciseDetailActivity.class.getName();
    public static final boolean  d_onClick = true;
    public static final boolean d_onSaveClicked = true;
    public static final boolean d_setClosing = true;
    public static final boolean d_onDataRequested =true;
    public static final boolean d_onPostResume = true;
    public static final boolean d_onStop = true;
    String groupId;
    String exerciseId;
    int groupIdInt;
    int exerciseIdInt;

    ExerciseDetailFragmentEdit editFragment;

    Fragment currentFragment;

    private ExerciseCollection collection;
    App app;


    FloatingActionButton fab;
    /** Networking stuff **/

    GoogleAccountCredential mCredential;
    ProgressDialog mProgress;

    private boolean isProcessing;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d(TAG, "onCreate: Enter");
        setContentView(R.layout.activity_exercise_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.detail_toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(Color.argb(0,255, 102, 0));


        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        //get the action bar
        fab = (FloatingActionButton) findViewById(R.id.fab);

        if (savedInstanceState == null) {

            //get the id
            exerciseId = getIntent().getStringExtra(ExerciseDetailFragment.ARG_EXERCISE_ID);
            groupId = getIntent().getStringExtra(ExerciseDetailFragment.ARG_GROUP_ID);


            Bundle arguments = new Bundle();
            arguments.putString(ExerciseDetailFragment.ARG_GROUP_ID, groupId);

            //if not editing, start the normal detail fragment,
            if (exerciseId != null && groupId != null) {

                exerciseIdInt = Integer.parseInt(exerciseId);
                groupIdInt = Integer.parseInt(groupId);

                arguments.putString(ExerciseDetailFragment.ARG_EXERCISE_ID, exerciseId);

                if (exerciseIdInt != -1 && groupIdInt != -1) {
                    fab.setVisibility(View.VISIBLE);
                    currentFragment = ExerciseDetailFragment.newInstance();
                    currentFragment.setArguments(arguments);
                    getSupportFragmentManager().beginTransaction()
                            .add(R.id.exercise_detail_container, currentFragment)
                            .commit();
                }
            }else{
                //else start the edit fragment
                fab.setVisibility(View.INVISIBLE);
                editFragment = new ExerciseDetailFragmentEdit();
                editFragment.setArguments(arguments);
                currentFragment = editFragment;
                getSupportFragmentManager().beginTransaction()
                        .add(R.id.exercise_detail_container, editFragment)
                        .commit();
            }

            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if(d_onClick) Log.e(TAG, "onClick() Click FAB");
                    fab.setVisibility(View.INVISIBLE);
                    Bundle arguments = new Bundle();
                    arguments.putString(ExerciseDetailFragment.ARG_EXERCISE_ID, exerciseId);
                    arguments.putString(ExerciseDetailFragment.ARG_GROUP_ID, groupId);
                    editFragment = new ExerciseDetailFragmentEdit();
                    editFragment.setArguments(arguments);
                    currentFragment = editFragment;
                    getSupportFragmentManager().beginTransaction()
                            .replace(R.id.exercise_detail_container, editFragment)
                            .addToBackStack(null)
                            .commit();
                }
            });
        }

        //selected group is always selected
        mProgress = new ProgressDialog(this);
        mProgress.setMessage("Fetching Exercise Details...");

        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(NetworkUtils.SCOPES))
                .setBackOff(new ExponentialBackOff())
                .setSelectedAccountName(App.fetchName(getApplicationContext()));
    }

    @Override
    protected void onPause() {
        super.onPause();
        mProgress.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveClicked() {
        if(d_onSaveClicked) Log.e(TAG, "onSaveClicked() SaveClicked!");
        fab.setVisibility(View.VISIBLE);
        onBackPressed();
    }

    /** network stuff **/

    /**
     * Called when an activity launched here (specifically, AccountPicker
     * and authorization) exits, giving you the requestCode you started it with,
     * the resultCode it returned, and any additional data from it.
     *
     * @param requestCode code indicating which activity result is incoming.
     * @param resultCode  code indicating the result of the incoming
     *                    activity result.
     * @param data        Intent (containing result data) returned by incoming
     *                    activity result.
     */
    @Override
    protected void onActivityResult(
            int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case NetworkUtils.REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    isGooglePlayServicesAvailable();
                }
                break;
            case NetworkUtils.REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        mCredential.setSelectedAccountName(accountName);
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(NetworkUtils.PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    Log.d(TAG, "onActivityResult: " + "Account unspecified.");
                }
                break;
            case NetworkUtils.REQUEST_AUTHORIZATION:
                if (resultCode != RESULT_OK) {
                    chooseAccount();
                }
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }


    /**
     * Attempt to get a set of data from the Google Apps Script Execution API to display. If the
     * email address isn't known yet, then call chooseAccount() method so the
     * user can pick an account.
     */
    private void refreshResults(String routine, String exercise) {
        if (mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else {
            if (isDeviceOnline()) {

                DetailRequestParams params = new DetailRequestParams();
                params.exercise = exercise;
                params.routine = routine;

                new FetchDetailTask(mCredential,routine,exercise)
                        .execute(params);
            } else {
                Log.d(TAG, "refreshResults: " + "No network connection available.");
                isProcessing= false;
            }
        }
    }


    /**
     * Starts an activity in Google Play Services so the user can pick an
     * account.
     */
    private void chooseAccount() {
        startActivityForResult(
                mCredential.newChooseAccountIntent(), NetworkUtils.REQUEST_ACCOUNT_PICKER);
    }

    /**
     * Checks whether the device currently has a network connection.
     *
     * @return true if the device has a network connection, false otherwise.
     */
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * Check that Google Play services APK is installed and up to date. Will
     * launch an error dialog for the user to update Google Play Services if
     * possible.
     *
     * @return true if Google Play Services is available and up to
     * date on this device; false otherwise.
     */
    private boolean isGooglePlayServicesAvailable() {
        final int connectionStatusCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        } else if (connectionStatusCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }

    /**
     * Display an error dialog showing that Google Play Services is missing
     * or out of date.
     *
     * @param connectionStatusCode code describing the presence (or lack of)
     *                             Google Play Services on this device.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                connectionStatusCode,
                ExerciseDetailActivity.this,
                NetworkUtils.REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    /**
     * An asynchronous task that handles the Google Apps Script Execution API call.
     * Placing the API calls in their own task ensures the UI stays responsive.
     */
    private class FetchDetailTask extends AsyncTask<DetailRequestParams, Void, String> {
        private com.google.api.services.script.Script mService = null;
        private Exception mLastError = null;

        private String routine;
        private String exercise;

        public FetchDetailTask(GoogleAccountCredential credential, String routine, String exercise) {

            this.routine = routine;
            this.exercise = exercise;

            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.script.Script.Builder(
                    transport, jsonFactory, NetworkUtils.setHttpTimeout(credential))
                    .setApplicationName("Import Routine")
                    .build();
        }

        /**
         * Background task to call Google Apps Script Execution API.
         *
         * @param params no parameters needed for this task.
         */
        @Override
        protected String doInBackground(DetailRequestParams... params) {
            try {
                return getDataFromApi(params[0]);
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        /**
         * Call the API to run an Apps Script function that returns a list
         * of folders within the user's root directory on Drive.
         *
         * @return list of String folder names and their IDs
         * @throws IOException
         */
        private String getDataFromApi(DetailRequestParams params)
                throws IOException, GoogleAuthException {
            // ID of the script to call. Acquire this from the Apps Script editor,
            // under Publish > Deploy as API executable.
            String scriptId = "MR371DZMGOwaZA0otZtnqoBnNlfM_PQae";

            //Load the parameters to send out
            List<Object> parameters = new ArrayList<>();
            parameters.add(params.routine);
            parameters.add(params.exercise);

            // Create an execution request object.
            ExecutionRequest request = new ExecutionRequest()
                    .setFunction("getExerciseData")
                    .setParameters(parameters);

            // Make the request.
            Operation op = mService.scripts().run(scriptId, request).execute();

            // Print results of request.
            if (op.getError() != null) {
                throw new IOException(getScriptError(op));
            }
            if (op.getResponse() != null &&
                    op.getResponse().get("result") != null) {
            }

            return (String)op.getResponse().get("result");
        }

        /**
         * Interpret an error response returned by the API and return a String
         * summary.
         *
         * @param op the Operation returning an error response
         * @return summary of error response, or null if Operation returned no
         * error
         */
        private String getScriptError(Operation op) {
            if (op.getError() == null) {
                return null;
            }

            // Extract the first (and only) set of error details and cast as a Map.
            // The values of this map are the script's 'errorMessage' and
            // 'errorType', and an array of stack trace elements (which also need to
            // be cast as Maps).
            Map<String, Object> detail = op.getError().getDetails().get(0);
            List<Map<String, Object>> stacktrace =
                    (List<Map<String, Object>>) detail.get("scriptStackTraceElements");

            java.lang.StringBuilder sb =
                    new StringBuilder("\nScript error message: ");
            sb.append(detail.get("errorMessage"));

            if (stacktrace != null) {
                // There may not be a stacktrace if the script didn't start
                // executing.
                sb.append("\nScript error stacktrace:");
                for (Map<String, Object> elem : stacktrace) {
                    sb.append("\n  ");
                    sb.append(elem.get("function"));
                    sb.append(":");
                    sb.append(elem.get("lineNumber"));
                }
            }
            sb.append("\n");
            return sb.toString();
        }

        @Override
        protected void onPreExecute() {
            if (isProcessing) {
                mProgress.show();
            }
        }

        @Override
        protected void onPostExecute(String output) {
            mProgress.hide();
            if (output == null ) {
//                Log.d(TAG, "onPostExecute: " + "No results returned.");
            } else {
//                Log.d(TAG, "onPostExecute: Fetching successful");
//                Log.d(TAG, "onPostExecute: " + output);

                App app = (App)getApplication();
                SharedPreferences cacheStatusPref = app.getCacheStatusPreferences();
                SharedPreferences cachePref       = app.getCachePreferences();

                Log.d(TAG, "onPostExecute: Placing:" + output);

                String key = CryptoUtils.MD5(routine, exercise);
                SharedPreferences.Editor edit = cachePref.edit();
                edit.putString(key, output);
                edit.apply();

//                Log.d(TAG, "onPostExecute: R:" +routine + " E:" + exercise );
//                Log.d(TAG, "onPostExecute: setting key " + key + " to false!");
                //update cache table
                SharedPreferences.Editor editor = cacheStatusPref.edit();
                editor.putBoolean(key, false);
                editor.apply();
                passDataToFragment(output);
            }
            isProcessing = false;
        }

        @Override
        protected void onCancelled() {
            mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            NetworkUtils.REQUEST_AUTHORIZATION);
                } else {
                    Log.d(TAG, "onCancelled: " + "The following error occurred:\n"
                            + mLastError.getMessage());
                }
            } else {
                Log.d(TAG, "onCancelled: " + "Request cancelled.");
            }
        }
    }

    private void loadCollection() {
        app = ((App)getApplicationContext());
        collection = App.LoadCollection(app.getPreferences(),app.getGson());
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        if(d_onPostResume) Log.e(TAG, "onPostResume() Enter");

        if (currentFragment instanceof ExerciseDetailFragment) {

            if (collection == null) {
                loadCollection();
            }

            ExerciseGroup group = collection.getGroups().get(groupIdInt);
            Exercise exercise = group.getExercises().get(exerciseIdInt);
            onDataRequested(group.getGroupName(), exercise.name);
        }
    }

    @Override
    public void onDataRequested(String routine, String exercise) {

            //check the cache,
            App app = (App)getApplication();
            SharedPreferences cacheStatusPref = app.getCacheStatusPreferences();
            SharedPreferences cachePref       = app.getCachePreferences();

            String key = CryptoUtils.MD5(routine, exercise);
//            Log.d(TAG, "onDataRequested: R:" + routine + " E:" + exercise);
//            Log.d(TAG, "onDataRequested: " + key + " is " +  cacheStatusPref.getBoolean(key,true));

            //cache shows new data available OR no data found, load from network
            if(cacheStatusPref.getBoolean(key,true)){
                requestFromNetwork(routine,exercise);
            }else{
                if(d_onDataRequested) Log.e(TAG, "onDataRequested() Loading from Cache");

                //no new data, load from cache
                String dataString = cachePref.getString(key, "");
                if (!dataString.isEmpty()) {
                    passDataToFragment(dataString);
                }else{
                    requestFromNetwork(routine, exercise);
                }
            }
    }

    public void passDataToFragment(String output){
        if (currentFragment instanceof ExerciseDetailFragment) {
            ((ExerciseDetailFragment)currentFragment).parseData(output);
        }
    }

    public void requestFromNetwork(String routine,String exercise){
        if(d_onDataRequested) Log.e(TAG, "onDataRequested() Loading From Network");
        isProcessing = true;
        if (isProcessing) {
            if (isGooglePlayServicesAvailable()) {
                refreshResults(routine,exercise);
            } else {
                Log.d(TAG, "onResume: " + "Google Play Services required: " +
                        "after installing, close and relaunch this app.");
                isProcessing = false;
            }
        }
    }




}
