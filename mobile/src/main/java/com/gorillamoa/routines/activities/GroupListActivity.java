package com.gorillamoa.routines.activities;

import android.app.DialogFragment;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.Toast;

import com.andexert.library.RippleView;
import com.gorillamoa.routines.R;
import com.gorillamoa.routines.app.App;
import com.gorillamoa.routines.appearance.ItemDecoration;
import com.gorillamoa.routines.data.Exercise;
import com.gorillamoa.routines.data.ExerciseCollection;
import com.gorillamoa.routines.data.ExerciseGroup;
import com.gorillamoa.routines.data.ExerciseProgress;
import com.gorillamoa.routines.dialogs.DeleteDialog;
import com.gorillamoa.routines.fragments.ExerciseDetailFragment;
import com.gorillamoa.routines.dialogs.NewGroupDialog;
import com.gorillamoa.routines.utils.CryptoUtils;

import java.util.List;

/**
 * An activity representing a list of Exercises. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ExerciseDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class GroupListActivity extends AppCompatActivity implements NewGroupDialog.NewGroupDialogListener,
        DeleteDialog.DeleteDialogListener {
    private static final String TAG = GroupListActivity.class.getName();
    public static final boolean d_onPostResume = false;
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;

    private SimpleItemRecyclerViewAdapter adapter;
    private View recyclerView;
    private View background;

    private ExerciseCollection collection;
    private App app;
    private int longSelectedItem;
    private String longSelectedName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_list);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        toolbar.setBackgroundColor(Color.rgb(255, 102, 0));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment dialog = new NewGroupDialog();
                dialog.show(getFragmentManager(), "NewGroupDialog");
            }
        });
        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        recyclerView = findViewById(R.id.exercise_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);
        ((RecyclerView) recyclerView).addItemDecoration(new ItemDecoration(50));

        if (findViewById(R.id.exercise_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

//        Drawable wallpaper = WallpaperManager.getInstance(this).getDrawable();
//        background = findViewById(R.id.group_list_root_layout);
//        background.setBackground(wallpaper);

    }

    @Override
    protected void onPostResume() {
        if(d_onPostResume) Log.e(TAG, "onPostResume() ");
        super.onPostResume();
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_group,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:

                Log.d(TAG, "onOptionsItemSelected: Home pressed");
                finish();
                return true;

            case R.id.action_delete_all_groups:

                loadRoutines();
                collection.getGroups().clear();
                collection.setProgress(new ExerciseProgress());
                App.saveCollection(collection,app.getPreferences(),app.getGson());
                adapter.setNewItems(collection.getGroups());
                adapter.notifyDataSetChanged();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        //FETCH Exercise items based on the given day
        loadRoutines();

        List<ExerciseGroup> items = collection.getGroups();
        if (adapter == null) {
            adapter = new SimpleItemRecyclerViewAdapter(items);


        } else {
            adapter.setNewItems(items);
            adapter.notifyDataSetChanged();
        }
        recyclerView.setAdapter(adapter);
        recyclerView.setLongClickable(true);
    }

    public class SimpleItemRecyclerViewAdapter extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private List<ExerciseGroup> mValues;
        public SimpleItemRecyclerViewAdapter(List<ExerciseGroup> items) {
            mValues = items;
        }
        public void setNewItems(List<ExerciseGroup> items) {
            mValues = items;
        }
        private boolean isLongPressed;

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.group_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.mItem = mValues.get(position);
            holder.mNameView.setText(mValues.get(position).getGroupName());
            holder.cardViewLayout.setBackgroundResource(returnResBasedOnClue(mValues.get(position).getGroupName()));
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.d(TAG, "onClick: Click");
                    isLongPressed = false;

//                    Log.d(TAG, "onClick: Clicked Item ID " + position);
//                    if (mTwoPane) {
//                        ExerciseDetailFragment fragment = new ExerciseDetailFragment();
//                        fragment.setArguments(arguments);
//                        getSupportFragmentManager().beginTransaction()
//                                .replace(R.id.exercise_detail_container, fragment)
//                                .commit();
//                    } else {
//                    }
                }
            });

            ((RippleView)holder.mView).setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
                @Override
                public void onComplete(RippleView rippleView) {

                    if (isLongPressed) {
                        longSelectedItem = position;
                        longSelectedName = mValues.get(position).getGroupName();

                        DialogFragment dialog = new DeleteDialog();
                        dialog.show(getFragmentManager(), "DeleteDialog");

                    }else{

                        Bundle arguments = new Bundle();
                        arguments.putString(ExerciseDetailFragment.ARG_GROUP_ID, "" + position);

                        Context context = rippleView.getContext();
                        Intent intent = new Intent(context, ExerciseListActivity.class);
                        intent.putExtras(arguments);
                        context.startActivity(intent);
                    }
                }
            });

            holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Log.d(TAG, "onClick: Long Click");
                    isLongPressed = true;
                    return true;
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            public final View mView;
            public final TextView mNameView;
            public final LinearLayout cardViewLayout;
            public final RippleView rippleView;
            public ExerciseGroup mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                rippleView = (RippleView) view.findViewById(R.id.ripple_view);
                rippleView.setRippleAlpha(150);
                rippleView.setRippleDuration(150);
                rippleView.setZooming(true);
                rippleView.setZoomDuration(150);
                mNameView = (TextView) view.findViewById(R.id.name);
                cardViewLayout = (LinearLayout) view.findViewById(R.id.card_view_layout);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mNameView.getText() + "'";
            }
        }


        public int returnResBasedOnClue(String in) {

            String string = in.trim().toLowerCase().replaceAll("[^a-zA-Z0-9]+", "");
            if (string.contains("back")) {
                return R.drawable.group_back;
            } else if (string.contains("arms") ||
                    string.contains("shoulders") ||
                    string.contains("upperbody")) {
                return R.drawable.group_shoulder;
            } else if (string.contains("stretch") ||
                    string.contains("yoga")) {
                return R.drawable.group_stretch;
            } else if (string.contains("butt") ||
                    string.contains("glute") ||
                    string.contains("measure")) {
                return R.drawable.group_butt;
            } else if (string.contains("cardio") ||
                    string.contains("swim")) {
                return R.drawable.group_cardio;
            } else if (string.contains("leg") ||
                    string.contains("lowerbody") ||
                    string.contains("calf")) {
                return R.drawable.group_leg;
            } else if (string.contains("deadlift") ||
                    string.contains("back")) {
                return R.drawable.group_lower_back;
            } else if (string.contains("abs") ||
                    string.contains("abdominal") ||
                    string.contains("stomach") ||
                    string.contains("crunches") ||
                    string.contains("chest")) {
                return R.drawable.group_abs_chest;
            } else {
                return R.drawable.group_lower_back;
            }
        }
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log.d(TAG, "onBackPressed: Back pressed");
    }

    @Override
    public void CreateClicked(String name) {
        //validate name
        if (name.isEmpty() || name.equals("")) {
            Toast.makeText(this, "Name cannot be blank!", Toast.LENGTH_LONG).show();
            return;
        }
        loadRoutines();

        //insert a new group
        ExerciseGroup group = new ExerciseGroup();
        group.setGroupName(name);
        collection.getGroups().add(group);
        App.saveCollection(collection, app.getPreferences(), app.getGson());

        //relaod the page
        adapter.setNewItems(collection.getGroups());
    }

    private void loadRoutines(){

        if (app == null) {
            app = (App) getApplicationContext();
        }

        //if collection is null, load it up,
        if (collection == null) {
            collection = App.LoadCollection(app.getPreferences(), app.getGson());
        }
    }


    @Override
    public void DeleteClicked() {
        loadRoutines();
        //insert a new group

        //delete any cached stuff
        ExerciseGroup group = collection.getGroups().get(longSelectedItem);
        App app = (App) getApplication();
        SharedPreferences cachePref = app.getCachePreferences();
        SharedPreferences cacheStatusPref = app.getCacheStatusPreferences();
        SharedPreferences.Editor cacheEditor = cachePref.edit();
        SharedPreferences.Editor cacheStatusEditor = cacheStatusPref.edit();

        for (Exercise exercise : group.getExercises()) {

            String key = CryptoUtils.MD5(group.getGroupName(), exercise.name);
            cacheEditor.putString(key, "");
            cacheStatusEditor.putBoolean(key, true);
        }

        cacheEditor.apply();
        cacheStatusEditor.apply();

        collection.getGroups().remove(longSelectedItem);
        App.saveCollection(collection, app.getPreferences(), app.getGson());
        adapter.setNewItems(collection.getGroups());
        adapter.notifyDataSetChanged();

        longSelectedItem = -1;
        longSelectedName = "";

    }

    @Override
    public String getSeletedName() {
        return longSelectedName;
    }
}
