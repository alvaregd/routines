package com.gorillamoa.routines.activities;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import android.support.v7.app.ActionBar;
import android.view.MenuItem;

import com.andexert.library.RippleView;
import com.gorillamoa.routines.R;

import com.gorillamoa.routines.appearance.ItemDecoration;
import com.gorillamoa.routines.data.Exercise;
import com.gorillamoa.routines.data.ExerciseGroup;
import com.gorillamoa.routines.data.ExerciseProgress;
import com.gorillamoa.routines.dialogs.DeleteDialog;
import com.gorillamoa.routines.fragments.ExerciseDetailFragment;
import com.gorillamoa.routines.app.App;
import com.gorillamoa.routines.data.ExerciseCollection;
import com.gorillamoa.routines.utils.CryptoUtils;

import java.util.List;

/**
 * An activity representing a list of Exercises. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link ExerciseDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class ExerciseListActivity extends AppCompatActivity implements DeleteDialog.DeleteDialogListener{
    private static final String TAG = ExerciseListActivity.class.getName();
    public static final boolean d_onClick = true;
    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private int selectedId;

    private SimpleItemRecyclerViewAdapter adapter;
    private View recyclerView;
    private View background;

    private int longSelectedIndex;
    private String longSelectedName;

    private App app;
    private ExerciseCollection collection;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_list);

        try{
            selectedId = Integer.valueOf(getIntent().getStringExtra(ExerciseDetailFragment.ARG_GROUP_ID));
            Log.d(TAG, "onCreate: selectedId " + selectedId);
        }catch (NumberFormatException e){
            Log.d(TAG, "onCreate: Number format exception: " + getIntent().getStringExtra(ExerciseDetailFragment.ARG_GROUP_ID));
        }

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        toolbar.setBackgroundColor(Color.rgb(255, 102, 0));

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ExerciseListActivity.this, ExerciseDetailActivity.class);
                intent.putExtra(ExerciseDetailFragment.ARG_GROUP_ID, ""+selectedId);
                startActivity(intent);
            }
        });
        // Show the Up button in the action bar.
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        recyclerView = findViewById(R.id.exercise_list);
        assert recyclerView != null;
        setupRecyclerView((RecyclerView) recyclerView);
        ((RecyclerView) recyclerView).addItemDecoration(new ItemDecoration(25));

        if (findViewById(R.id.exercise_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }

        background = findViewById(R.id.group_list_root_layout);
        background.setBackgroundResource(R.drawable.bg_abs);
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        assert recyclerView != null;
        setupRecyclerView((RecyclerView)recyclerView);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_exercise_list,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()){
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_remove_exercise:

                loadRoutines();
                collection.getGroups().get(selectedId).getExercises().clear();
                if (selectedId == collection.getProgress().getCurrentGoup()) {
                    collection.setProgress(new ExerciseProgress());
                }
                App.saveCollection(collection, app.getPreferences(), app.getGson());
                adapter.setNewItems(collection.getGroups().get(selectedId).getExercises());
                adapter.notifyDataSetChanged();

                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setupRecyclerView(@NonNull RecyclerView recyclerView) {
        //FETCH Exercise items based on the given day
        App app = (App)getApplicationContext();
        ExerciseCollection collection = App.LoadCollection(app.getPreferences(), app.getGson());
        ExerciseGroup group = collection.getExerciseGroup(selectedId);
        List<Exercise> items = group.getExercises();

        if (adapter == null) {
            adapter = new SimpleItemRecyclerViewAdapter(items);
        }else {
            adapter.setNewItems(items);
            adapter.notifyDataSetChanged();
        }

        recyclerView.setAdapter(adapter);
    }

    public class SimpleItemRecyclerViewAdapter extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {
        private List<Exercise> mValues;
        boolean isLongPressed;

        public SimpleItemRecyclerViewAdapter(List<Exercise> items) {
            mValues = items;
        }
        public void setNewItems(List<Exercise> items) {
            mValues = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.exercise_list_content, parent, false);
            return new ViewHolder(view);
        }


        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.mItem = mValues.get(position);
            holder.mIdView.setText(String.valueOf(position + 1));
            holder.mNameView.setText(mValues.get(position).name);
            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    isLongPressed = false;
                }
            });

            holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    isLongPressed = true;
                    return true;
                }
            });

            ((RippleView)holder.mView).setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
                @Override
                public void onComplete(RippleView rippleView) {

                    if (isLongPressed) {
                        longSelectedIndex = position;
                        longSelectedName = mValues.get(position).name;

                        DialogFragment dialog = new DeleteDialog();
                        dialog.show(getFragmentManager(),"DeleteExercise");

                    }else{
                        if(d_onClick) Log.e(TAG, "onClick() EXERCISE" + holder.mItem.id);
                        if(d_onClick) Log.e(TAG, "onClick() GROUP" + selectedId);

                        Bundle arguments = new Bundle();
                        arguments.putString(ExerciseDetailFragment.ARG_EXERCISE_ID, holder.mItem.id);
                        arguments.putString(ExerciseDetailFragment.ARG_GROUP_ID, ""+selectedId);

                        if (mTwoPane) {
                            ExerciseDetailFragment fragment = new ExerciseDetailFragment();
                            fragment.setArguments(arguments);
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.exercise_detail_container, fragment)
                                    .commit();
                        } else {
                            Context context = rippleView.getContext();
                            Intent intent = new Intent(context, ExerciseDetailActivity.class);
                            intent.putExtras(arguments);
                            context.startActivity(intent);
                        }
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mIdView;
            public final TextView mNameView;
            public final RippleView rippleView;

            public Exercise mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                rippleView = (RippleView) view.findViewById(R.id.ripple_view);
                rippleView.setRippleAlpha(150);
                rippleView.setRippleDuration(150);
                rippleView.setZooming(true);
                rippleView.setZoomDuration(150);
                mIdView = (TextView) view.findViewById(R.id.id);
                mNameView = (TextView) view.findViewById(R.id.name);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mNameView.getText() + "'";
            }
        }
    }

    @Override
    public void DeleteClicked() {

        loadRoutines();

        //delete any cached stuff
        ExerciseGroup group = collection.getGroups().get(selectedId);
        App app = (App) getApplication();
        SharedPreferences cachePref = app.getCachePreferences();
        SharedPreferences cacheStatusPref = app.getCacheStatusPreferences();
        SharedPreferences.Editor cacheEditor = cachePref.edit();
        SharedPreferences.Editor cacheStatusEditor = cacheStatusPref.edit();

        Exercise exercise = group.getExercises().get(longSelectedIndex);

        String key = CryptoUtils.MD5(group.getGroupName(), exercise.name);
        cacheEditor.putString(key, "");
        cacheStatusEditor.putBoolean(key, true);

        cacheEditor.apply();
        cacheStatusEditor.apply();

        //insert a new group
        collection.getGroups().get(selectedId).getExercises().remove(longSelectedIndex);
        App.saveCollection(collection, app.getPreferences(), app.getGson());
        adapter.setNewItems(collection.getGroups().get(selectedId).getExercises());
        adapter.notifyDataSetChanged();

        longSelectedIndex = -1;
        longSelectedName = "";
    }

    private void loadRoutines(){

        if (app == null) {
            app = (App) getApplicationContext();
        }

        //if collection is null, load it up,
        if (collection == null) {
            collection = App.LoadCollection(app.getPreferences(), app.getGson());
        }
    }


    @Override
    public String getSeletedName() {
        return longSelectedName;
    }

}
