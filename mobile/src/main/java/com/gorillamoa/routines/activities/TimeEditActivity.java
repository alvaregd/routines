package com.gorillamoa.routines.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.gorillamoa.routines.R;
import com.gorillamoa.routines.app.App;
import com.gorillamoa.routines.data.FakeTime;

public class TimeEditActivity extends AppCompatActivity implements View.OnClickListener {

    Button nextDay, previousDay;
    Button nextDayOfYear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_time_edit);
        nextDay = (Button)findViewById(R.id.b_Next_day);
        previousDay = (Button)findViewById(R.id.b_Previous_Day);
        nextDayOfYear = (Button) findViewById(R.id.b_next_day_of_year);

        nextDay.setOnClickListener(this);
        previousDay.setOnClickListener(this);
        nextDayOfYear.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
   /*     App app;
        FakeTime fakeTimeObject;
        app = ((App)getApplication());
        fakeTimeObject = app.getFakeTimeObject();

        switch (v.getId()) {
            case R.id.b_Next_day:
                fakeTimeObject.dayOfWeek++;
                if (fakeTimeObject.dayOfWeek > 7) {
                    fakeTimeObject.dayOfWeek = 1;
                }
                Toast.makeText(TimeEditActivity.this, "Day: "  + fakeTimeObject.dayOfWeek, Toast.LENGTH_SHORT).show();
                break;
            case R.id.b_Previous_Day:
                fakeTimeObject.dayOfWeek--;
                if (fakeTimeObject.dayOfWeek < 1) {
                    fakeTimeObject.dayOfWeek = 7;
                }
                Toast.makeText(TimeEditActivity.this, "Day: "  + fakeTimeObject.dayOfWeek, Toast.LENGTH_SHORT).show();
                break;
            case R.id.b_next_day_of_year:
                fakeTimeObject.dayOfYear++;
                Toast.makeText(TimeEditActivity.this, "Day(year): "  + fakeTimeObject.dayOfYear, Toast.LENGTH_SHORT).show();
                break;
        }
        App.saveFakeTime(fakeTimeObject, app.getPreferences(), app.getGson());*/
    }
}
