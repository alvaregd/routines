package com.gorillamoa.routines.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.gorillamoa.routines.R;

import java.util.Random;

public class MainMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        View view = findViewById(R.id.fragment);

        Random rand = new Random();
        int n = rand.nextInt(6) + 1;

        switch (n) {
            case 1:
                view.setBackgroundResource(R.drawable.menu_1);
                break;
            case 2:
                view.setBackgroundResource(R.drawable.menu_3);
                break;
            case 3:
                view.setBackgroundResource(R.drawable.menu_4);
                break;
            case 4:
                view.setBackgroundResource(R.drawable.menu_5);
                break;
            case 5:
                view.setBackgroundResource(R.drawable.menu_4);
                break;
            case 6:
                view.setBackgroundResource(R.drawable.menu_1);
                break;
            default:
                view.setBackgroundResource(R.drawable.menu_1);
                break;
        }
    }
}