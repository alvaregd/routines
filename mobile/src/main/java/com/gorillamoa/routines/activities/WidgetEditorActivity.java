package com.gorillamoa.routines.activities;

import android.app.WallpaperManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.gorillamoa.routines.R;
import com.gorillamoa.routines.app.App;
import com.gorillamoa.routines.data.AppearanceSettings;

import org.w3c.dom.Text;

public class WidgetEditorActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    private static final String TAG = WidgetEditorActivity.class.getName();
    private static final boolean d_saveBackgroundColour = true;
    private static final boolean d_saveTextColour = true;
    private static final boolean d_onWindowFocusChanged = true;
    private static final boolean d_calculateBackgroundColour = true;
    private static final boolean d_onCheckedChanged = true;
    private static final boolean d_calculateTextColour = true;

    private boolean isStartingUp =true;
    private boolean isTransitioning = false;

    LinearLayout background;
    LinearLayout container;
    SeekBar sbHue, sbSaturation, sbValue, sbAlpha;
    RadioButton rbText, rbBackground;

    ImageView backgroundView;
    RectF rectangle;
    Paint p = new Paint();
    final float roundDp = 5;
    Bitmap bitmap;
    Canvas canvas;
    LinearLayout widgeView;

    float[] hsv = new float[3];
    int alpha;
    int resultColour;

    final static int HUE = 0;
    final static int SATURATION = 1;
    final static int VALUE = 2;

    AppearanceSettings settings;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                if (rbBackground.isChecked()) saveBackgroundColour();
                if (rbText.isChecked()) saveTextColour();

                App.saveAppearanceSettings(settings,
                        ((App) getApplicationContext()).getPreferences(),
                        ((App) getApplicationContext()).getGson());
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void saveBackgroundColour() {
        if (d_saveBackgroundColour) Log.e(TAG, "saveBackgroundColour() ");

        settings.homeWidgetBackgroundColour = resultColour;
        settings.hueProgressBackground = sbHue.getProgress();
        settings.alphaProgressBackground = sbAlpha.getProgress();
        settings.valueProgressBackground = sbValue.getProgress();
        settings.saturationProgressBackground = sbSaturation.getProgress();
    }

    public void saveTextColour() {
        if (d_saveTextColour) Log.e(TAG, "saveTextColour() ");
        settings.homeWidgetTextColour = resultColour;
        settings.hueProgressText = sbHue.getProgress();
        settings.alphaProgressText = sbAlpha.getProgress();
        settings.valueProgressText = sbValue.getProgress();
        settings.saturationProgressText = sbSaturation.getProgress();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflatoer = getMenuInflater();
        inflatoer.inflate(R.menu.menu_edit, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_widget_editor);
        isStartingUp = true;

        rbText = (RadioButton) findViewById(R.id.rb_text);
        rbBackground = (RadioButton) findViewById(R.id.rb_background);
        rbText.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && !isStartingUp) {
                    if(d_onCheckedChanged) Log.e(TAG, "onCheckedChanged() rbTextChecked");
                    saveBackgroundColour();
                    calculateTextColour();
                }
            }
        });

        rbBackground.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && !isStartingUp) {
                    if(d_onCheckedChanged) Log.e(TAG, "onCheckedChanged() rbBackgroundChecked");
                    saveTextColour();
                    calculateBackgroundColour();
                }
            }
        });

        settings = ((App) getApplicationContext()).getWidgetAppearanceSettings();

        resultColour = settings.homeWidgetBackgroundColour;

        Drawable wallpaper = WallpaperManager.getInstance(this).getDrawable();
        background = (LinearLayout) findViewById(R.id.sv_background);
        background.setBackground(wallpaper);

        container = (LinearLayout) findViewById(R.id.container);
        LayoutInflater inflater = LayoutInflater.from(this);
        widgeView = (LinearLayout) inflater.inflate(R.layout.widget_standard, container);

        backgroundView = (ImageView) widgeView.findViewById(R.id.background);

        sbHue = (SeekBar) findViewById(R.id.sb_hue);
        sbSaturation = (SeekBar) findViewById(R.id.sb_saturation);
        sbAlpha = (SeekBar) findViewById(R.id.sb_alpha);
        sbValue = (SeekBar) findViewById(R.id.sb_lightness);

        sbHue.setOnSeekBarChangeListener(this);
        sbValue.setOnSeekBarChangeListener(this);
        sbAlpha.setOnSeekBarChangeListener(this);
        sbSaturation.setOnSeekBarChangeListener(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (!isTransitioning) {

            switch (seekBar.getId()) {
                case R.id.sb_hue:
                    hsv[HUE] = (360f * ((float) progress / (float) 100));
                    break;
                case R.id.sb_saturation:
                    hsv[SATURATION] = (((float) progress / (float) 100));
                    break;
                case R.id.sb_lightness:
                    hsv[VALUE] = (((float) progress / (float) 100));
                    break;
                case R.id.sb_alpha:
                    alpha = (int) (255 * ((float) progress / (float) 100));
                    break;
            }
            calculateColourIntegerAndApply();
        }
    }

    private void calculateTextColour() {
        if(d_calculateTextColour) Log.e(TAG, "calculateTextColour() enter");
        isTransitioning = true;
        sbHue.setProgress(settings.hueProgressText);
        sbSaturation.setProgress(settings.saturationProgressText);
        sbAlpha.setProgress(settings.alphaProgressText);
        sbValue.setProgress(settings.valueProgressText);
        isTransitioning = false;
        calculateHSVAvalues();
    }



    private void calculateBackgroundColour() {
        if(d_calculateBackgroundColour) Log.e(TAG, "calculateBackgroundColour() ");
        isTransitioning = true;
        sbHue.setProgress(settings.hueProgressBackground);
        sbSaturation.setProgress(settings.saturationProgressBackground);
        sbAlpha.setProgress(settings.alphaProgressBackground);
        sbValue.setProgress(settings.valueProgressBackground);
        isTransitioning = false;
        if(d_calculateBackgroundColour) Log.e(TAG, "calculateBackgroundColour() Set a bunch of progresses" + settings.hueProgressBackground);
        calculateHSVAvalues();
    }

    private void calculateHSVAvalues(){
        this.alpha = (int) (255 * ((float) sbAlpha.getProgress() / (float) 100));
        hsv[HUE] =  (360f * ((float) sbHue.getProgress() / (float) 100));
        hsv[SATURATION] = (((float) sbSaturation.getProgress() / (float) 100));
        hsv[VALUE] =  (((float) sbValue.getProgress()/ (float) 100));
        calculateColourIntegerAndApply();
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    private void calculateColourIntegerAndApply() {

        resultColour = Color.HSVToColor(alpha, hsv);
        if (rbBackground.isChecked()) {
            Log.d(TAG, "calculateColourIntegerAndApply: rbBackgroundChecked");
            applyChangetoWidgetBackground();
        } else if (rbText.isChecked()) {
            applyChangetoTextViews();
        }
    }

    private void applyChangetoTextViews() {
        findAllTextViews(widgeView, resultColour);
    }


    private void applyChangetoWidgetBackground() {

        //define our rectangle
        if (rectangle == null) {
            rectangle = new RectF(0, 0, backgroundView.getWidth(), backgroundView.getWidth());
        } else {
            rectangle.set(0, 0, backgroundView.getWidth(), backgroundView.getWidth());
        }

        if (bitmap == null) {
            bitmap = Bitmap.createBitmap(backgroundView.getWidth(), backgroundView.getHeight(), Bitmap.Config.ARGB_8888);
            canvas = new Canvas(bitmap);
        }

        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        LinearGradient gradient = new LinearGradient(0, 0, 0, 400, resultColour, resultColour, Shader.TileMode.CLAMP);
        p.setDither(true);
        p.setShader(gradient);

        canvas.drawRoundRect(rectangle, roundDp, roundDp, p);
        backgroundView.setImageBitmap(bitmap);
    }


    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        if(d_onWindowFocusChanged) Log.e(TAG, "onWindowFocusChanged() Checking Hue" + settings.hueProgressBackground);

        if (rbBackground.isChecked()) {
            if (d_onWindowFocusChanged) Log.e(TAG, "onWindowFocusChanged() Background Checked");
            calculateBackgroundColour();
            isStartingUp = false;
        } else if (rbText.isChecked()) {
            if (d_onWindowFocusChanged) Log.e(TAG, "onWindowFocusChanged() text checked");
            calculateTextColour();
            isStartingUp = false;
        }
    }

    private void findAllTextViews(ViewGroup viewGroup, int color) {

        int count = viewGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            View view = viewGroup.getChildAt(i);
            if (view instanceof ViewGroup)
                findAllTextViews((ViewGroup) view,color);
            else if (view instanceof TextView) {
                ((TextView) view).setTextColor(color);
            }
        }

    }

    //useless implements
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
