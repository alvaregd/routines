package com.gorillamoa.routines.data;

import android.util.Log;

/**
 * Created by alvaregd on 26/01/16.
 */
public class Exercise {
    private static final String TAG = Exercise.class.getName();

    public final static int ATTRIB_MAX_REST = 1;

    public String id;
    public int dayOfWeek;
    public String name;
    public ExerciseType type;
    public int sets;
    public int repsMin;
    public int repsMax;
    public float weight;
    public float weightIncrement;
    public int repIncrement;
    public boolean isWeightIncrement;
    public boolean isRepIncrement;
    public int attemps; ///used to calculate the current steps
    public String note;
    public int attribute0;//TODO make this a list of attrib objects
    public int value0;//TODO make this a list of attrib objects

    public enum ExerciseType {
        Timing,
        Standard
    }

    /**
     * Creates a new exercie data object
     * @param id        the id of the object
     * @param name      of  the exercise
     * @param type      the type of exercier (standard or Timing)
     * @param sets      the number of required sets to perform
     * @param repsMin   the starting rep count
     * @param repsMax   the max rep count
     * @param weight    the amount of weight to elft
     * @param dayOfWeek the day this exercise needs to be performed
     */
    public Exercise(
            String id,
            String name,
            ExerciseType type,
            int sets,
            int repsMin,
            int repsMax,
            int repIncrement,
            float weight,
            float weightIncrement,
            int dayOfWeek,
            boolean isRepIncrement,
            boolean isWeightIncrement,
            String note) {

        this.id = id;
        this.name = name;
        this.type = type;
        this.sets = sets;
        this.repsMin = repsMin;
        this.repsMax = repsMax;
        this.weight = weight;
        this.dayOfWeek = dayOfWeek;
        this.repIncrement = repIncrement;
        this.weightIncrement = weightIncrement;
        this.isRepIncrement = isRepIncrement;
        this.isWeightIncrement = isWeightIncrement;
        this.note = note;
        this.attemps = 0;
    }

    public float getWeight() {

        if (isWeightIncrement && isRepIncrement) {

            int repStep = 0;
            int steps = attemps;
            float currentWeight = weight;

            while (steps > 0) {

                if (repStep < (repsMax - repsMin)) {
                    repStep += repIncrement;
                } else {
                    repStep = 0;
                    currentWeight += weightIncrement;
                }
                steps--;
            }
            return currentWeight;
        } else if (isWeightIncrement) {

            return weight + (attemps * weightIncrement);

        } else {
            return weight;
        }
    }

    public int getRep() {

        if (isWeightIncrement && isRepIncrement) {

            int repStep = 0;
            int steps = attemps;

            while (steps > 0) {

                if (repStep < (repsMax - repsMin)) {
                    repStep += repIncrement;
                } else {
                    repStep = 0;
                }
                steps--;
            }
            return repsMin + repStep;

        } else if (isRepIncrement) {
            return repsMin + (attemps * repIncrement);
        } else {
            return repsMin;
        }
    }

    public void incrementAttemps() {
        this.attemps++;
    }

    public void resetAttemps() {
        this.attemps = 0;
    }

    public void setStartingWeightValue(float weight) {
        this.weight = weight;
        resetAttemps();
    }

    public void setStartingRepValue(int reps) {
        float tempWeightValue = this.getWeight();
        int repDifference = (repsMax - repsMin);
        this.repsMin = reps;
        this.repsMax = repsMin + repDifference;
        resetAttemps();
        this.weight = tempWeightValue;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean isFinishedExercise(int inSet){

        Log.d(TAG, "isFinishedExercise() inSet" + inSet  + " Sets: " + sets);
        return inSet >= this.sets;
    }

}

