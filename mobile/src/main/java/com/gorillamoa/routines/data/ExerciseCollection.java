package com.gorillamoa.routines.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alvaregd on 12/11/15.
 * Data object to hold exercise information
 */
public class ExerciseCollection {

   private List<ExerciseGroup> groups;
   private ExerciseProgress progress;

   public ExerciseCollection() {
      this.groups = new ArrayList<>();
   }

   public List<ExerciseGroup> getGroups() {
      return groups;
   }

   public void setGroups(List<ExerciseGroup> groups) {
      this.groups = groups;
   }

   public ExerciseProgress getProgress() {
      return progress;
   }


   public void setProgress(ExerciseProgress progress) {
      this.progress = progress;
   }

   public void init() {}

   public ExerciseGroup getExerciseGroup(int i){
      return groups.get(i);
   }

   public int getGroupCount(){
      return groups.size();
   }

   public void generateFakeData(){

      groups = new ArrayList<>();
      for (int i = 0; i < 5; i++) {
         ExerciseGroup group = new ExerciseGroup();
         group.generateFakeData(i);
         groups.add(group);
      }
      progress = new ExerciseProgress();
   }
}
