package com.gorillamoa.routines.data;

import java.util.Calendar;

/**
 * Created by alvaregd on 14/12/15.
 */
public class FakeTime {

    public int dayOfWeek;
    public int dayOfYear;

    public FakeTime() {
        Calendar date = Calendar.getInstance();
        dayOfWeek =  date.get(Calendar.DAY_OF_WEEK);
        dayOfYear = date.get(Calendar.DAY_OF_YEAR);
    }
}
