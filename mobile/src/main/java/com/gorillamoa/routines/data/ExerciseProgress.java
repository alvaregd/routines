package com.gorillamoa.routines.data;

/**
 * Created by alvaregd on 12/11/15.
 */
public class ExerciseProgress {

    private int currentGoup;
    private int currentExercise;    //the exercise that the person is one
    private int currentSet;         //the set that the person is on
    private int progress;           //tells the curent day's progress, used to calculate percentage
    private int totalGroupReps;
    private boolean finishRecords[];
    private int editRepValue = -1;      //a place to store the value when editing from the widget
    private float editWeightValue = -1; //a place to store the value when editing from the widget
    private int timeRemaining; //The amount of time remaining
    private long timeElapsed ;
    private boolean isFinishedRoutine;

    /*** GETTERS AND SETTERS*/
    public int getTimeRemaining() {
        return timeRemaining;
    }
    public void setTimeRemaining(int timeRemaining) {
        this.timeRemaining = timeRemaining;
    }
    public long getTimeElapsed() {return timeElapsed;}
    public void setTimeElapsed(long timeElapsed) {this.timeElapsed = timeElapsed;}
    public int getCurrentGoup() {
        return currentGoup;
    }
    public int getTotalGroupReps() {return totalGroupReps;}
    public void setTotalGroupReps(int totalGroupReps) {
        this.totalGroupReps = totalGroupReps;
    }
    public void setCurrentGoup(int currentGoup) {
        this.currentGoup = currentGoup;
    }
    public boolean[] getFinishRecords() {
        return finishRecords;
    }
    public void setFinishRecords(boolean[] finishRecords) {
        this.finishRecords = finishRecords;
    }
    public int getCurrentExercise() {
        return currentExercise;
    }
    public void setCurrentExercise(int currentExercise) {
        this.currentExercise = currentExercise;
    }
    public int getCurrentSet() {
        return currentSet;
    }
    public void setCurrentSet(int currentSet) {
        this.currentSet = currentSet;
    }
    public int getProgress() {
        return progress;
    }
    public void setProgress(int progress) {
        this.progress = progress;
    }
    public int getEditRepValue() {
        return editRepValue;
    }
    public void setEditRepValue(int editRepValue) {
        this.editRepValue = editRepValue;
    }
    public float getEditWeightValue() {
        return editWeightValue;
    }
    public void setEditWeightValue(float editWeightValue) {this.editWeightValue = editWeightValue; }
    public boolean isFinishedRoutine() {return isFinishedRoutine;}
    public void setFinishedRoutine(boolean finishedRoutine) {isFinishedRoutine = finishedRoutine;}

    public boolean isFinished(){


        for (boolean record  :finishRecords) {
            if(!record)return false;
        }

        //mark the time for right now
        if(!isFinishedRoutine){
            timeElapsed = System.currentTimeMillis() - timeElapsed;
            isFinishedRoutine = true;
        }
        return true;
    }
    
    public float getPercentageProgress(){
        return (float) progress / totalGroupReps;
    }

    public boolean isActive(){
        return currentGoup != -1;
    }

    public void advance(){

        this.currentSet++;
        this.progress++;
    }

    public void nextExercise(){

        //mark this exercis done
        this.finishRecords[currentExercise] = true;
        this.currentSet = 0;

        //move forward until we find an unfinished exercise
        while (finishRecords[currentExercise]) {

            //while moving forward, if we reach then end, fall back to the first unfinished exercise
            if (currentExercise == finishRecords.length -1 ) {

                for (int i = 0; i < finishRecords.length; i++) {
                    if (!finishRecords[i]) {
                        currentExercise = i;
                        break;
                    }
                }
                //If we reach here, it means all exercises are done
                return;
            }else{
                this.currentExercise++;
            }
        }
    }

    public ExerciseProgress() {
        currentExercise = 0;    //the exercise that the person is one
        currentSet = 0;         //the set that the person is on
        progress = 0;           //tells the curent day's progress, used to calculate percentage
        editRepValue = -1;      //a place to store the value when editing from the widget
        editWeightValue = -1; //a place to store the value when editing from the widget
        timeRemaining = 0;
    }

    public void setRoutine(int id, int size, int totalReps) {

        this.isFinishedRoutine = false;
        this.currentGoup = id;
        this.currentExercise = 0;
        this.currentSet= 0;
        this.progress = 0;
        this.editRepValue = -1;      //a place to store the value when editing from the widget
        this.editWeightValue = -1; //a place to store the value when editing from the widget
        this.timeRemaining = 0;
        this.finishRecords = new boolean[size];
        this.totalGroupReps = totalReps;
        this.timeElapsed = System.currentTimeMillis();
    }

    public String getWorkoutTime(){

        int hours = (int) this.timeElapsed / ( 60 * 60 * 1000);
        int minutes =(int)this.timeElapsed / (60 * 1000) % 60;

        return "Time: " + hours + " hrs " + minutes + " mins";
    }
}
