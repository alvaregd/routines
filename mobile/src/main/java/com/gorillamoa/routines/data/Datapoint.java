package com.gorillamoa.routines.data;

/**
 * Created by alvaregd on 15/11/15.
 */
public class Datapoint {

    public static final String PARAM_DATE   = "date"  ;
    public static final String PARAM_NAME   = "name"  ;
    public static final String PARAM_ROUTINE = "routine"   ;
    public static final String PARAM_WEIGHT = "weight";
    public static final String PARAM_REPS   = "repsMin"  ;

    public String date;
    public String routine;
    public String name;
    public float weight;
    public int reps;

}
