package com.gorillamoa.routines.data;

import android.util.Log;

/**
 * Created by alvaregd on 08/02/16.
 * object to hold fetched data.
 * also provides functionality to fetch data in various ways
 */
public class FetchedDataPoint {
    private static final String TAG = FetchedDataPoint.class.getName();

    public String date;
    public String[] reps;
    public String[] weights;

    public int getTotalSets(){
        try {
            return reps.length;
        } catch (NullPointerException e) {
            Log.d(TAG, "getTotalSets: ", e);
            return 0;
        }
    }

    public float getAverageWeight(){
        try {
            float averageWeight = 0.0f;
            if ( weights != null) {
                for (int i = 0; i < weights.length; i++) {
                    averageWeight += Float.valueOf(weights[i]);
                }
                averageWeight = averageWeight/weights.length;
            }
            return averageWeight;
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public float getAverageRep(){

        try {
            float averageRep = 0.0f;
            if ( reps != null) {
                for (int i = 0; i < reps.length; i++) {
                    averageRep += Float.valueOf(reps[i]);
                }
                averageRep = averageRep/reps.length;
            }
            return averageRep;
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public float getVolume(){
        try {
            float volume = 0.0f;
            if (reps != null && weights != null) {
                for (int i = 0; i < reps.length; i++) {
                    volume += Integer.valueOf(reps[i]) * Float.valueOf(weights[i]);
                }
            }
            return volume;
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public float getPerformance(int i) {

        try {
            float performanceValue = 0.0f;
            if (reps != null && weights != null) {
                performanceValue = Integer.valueOf(reps[i]) * Float.valueOf(weights[i]);
            }
            return performanceValue;
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public float getWeightbyIndex(int i){

        try {
            return Float.valueOf(weights[i]);
        } catch (ArrayIndexOutOfBoundsException e) {
            Log.d(TAG, "getWeightbyIndex: ",e);
            return 0.0f;
        } catch (NumberFormatException e) {
            Log.d(TAG, "getWeightbyIndex: ",e);
            return 0.0f;
        } catch (NullPointerException e) {
            Log.d(TAG, "getWeightbyIndex: ",e);
            return 0.0f;
        }
    }

    public float getRepbyIndex(int i){
        try {
            return Float.valueOf(reps[i]);
        } catch (ArrayIndexOutOfBoundsException e) {
            Log.d(TAG, "getWeightbyIndex: ",e);
            return 0.0f;
        } catch (NumberFormatException e) {
            Log.d(TAG, "getWeightbyIndex: ",e);
            return 0.0f;
        } catch (NullPointerException e) {
            Log.d(TAG, "getWeightbyIndex: ",e);
            return 0.0f;
        }
    }

    public float getHighestWeight(){

        try {
            float highestWeight = 0.0f;
            if (weights != null) {
                for (int i = 0; i < weights.length ; i++) {
                    float weight = Float.valueOf(weights[i]);
                    highestWeight = weight > highestWeight ? weight:highestWeight;
                }
            }
            return highestWeight;
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public int getTotalReps(){

        try {
            int totalReps = 0;
            if (reps != null) {
                for (int i = 0; i < reps.length; i++) {
                    totalReps += Integer.valueOf(reps[i]);
                }
                return totalReps;
            }else{
                return 0;
            }
        } catch (NumberFormatException e) {
            Log.d(TAG, "getTotalReps: ",e);
            return 0;
        }
    }

    public float getTotalWeight(){

        try {
            float totalWeight = 0;
            if (reps != null) {
                for (int i = 0; i < reps.length; i++) {
                    totalWeight += Float.valueOf(weights[i]);
                }
                return totalWeight;
            }else{
                return 0.0f;
            }
        } catch (NumberFormatException e) {
            Log.d(TAG, "getTotalReps: ",e);
            return 0.0f;
        }
    }

}
