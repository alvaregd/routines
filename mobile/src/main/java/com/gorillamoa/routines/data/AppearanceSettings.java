package com.gorillamoa.routines.data;


/**
 * Created by alvaregd on 19/11/15.
 * Determines how widget background should look like
 */
public class AppearanceSettings {

    public int lockWidgetBackgroundColour;
    public int lockWidgetTextColour;
    public int homeWidgetBackgroundColour;
    public int homeWidgetTextColour;

    public int hueProgressBackground;
    public int saturationProgressBackground;
    public int valueProgressBackground;
    public int alphaProgressBackground;

    public int hueProgressText;
    public int saturationProgressText;
    public int valueProgressText;
    public int alphaProgressText;

}
