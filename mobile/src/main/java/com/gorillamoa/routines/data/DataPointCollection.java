package com.gorillamoa.routines.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alvaregd on 15/12/15.
 * class structure of data point storage
 */
public class DataPointCollection {

    List<Datapoint> points;

    public List<Datapoint> getPoints() {
        return points;
    }
    public void setPoints(List<Datapoint> points) {
        this.points = points;
    }

    public void init() {
        points= new ArrayList<>();
    }

}
