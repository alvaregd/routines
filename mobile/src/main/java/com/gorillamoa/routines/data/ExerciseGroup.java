package com.gorillamoa.routines.data;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alvaregd on 26/01/16.
 * A simple class to store exercise group information
 */
public class ExerciseGroup {

    private List<Exercise> exercises;
    private String groupName;

    public ExerciseGroup() {
        exercises = new ArrayList<>();
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public List<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(List<Exercise> exercises) {
        this.exercises = exercises;
    }

    public void generateFakeData(int i) {

        groupName = "Group " + i;
        exercises = new ArrayList<>();
        for (int j = 0; j < i + 1; j++) {
            Exercise exercise = new Exercise(
                    ""+j,
                    "Exercise " + j,
                    Exercise.ExerciseType.Standard,
                    3,
                    j,
                    j,
                    j,
                    j,
                    j,
                    j,
                    true,
                    true,
                    "Exercise Note " + j
            );
            exercises.add(exercise);
        }
    }

    public int getTotalGroupSets(){

        int totalReps = 0;
        for (Exercise exercise : exercises) {
            totalReps += exercise.sets;
        }
        return totalReps;
    }
}
