package com.gorillamoa.routines.providers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;
import android.widget.Toast;

import com.gorillamoa.routines.R;
import com.gorillamoa.routines.app.App;
import com.gorillamoa.routines.data.ExerciseCollection;

/**
 * Created by alvaregd on 26/01/16.
 */
public class ListProvider implements RemoteViewsService.RemoteViewsFactory {

    private static final String TAG = ListProvider.class.getName();

    ExerciseCollection collection;

    private Context context = null;
    private int appWidgetId;

    public ListProvider(Context context, Intent intent) {
        this.context = context;

        App app = ((App) context.getApplicationContext());
        collection = App.LoadCollection(app.getPreferences(), app.getGson());

        if (collection == null) {
            Toast.makeText(context, "Unable to Load Data!", Toast.LENGTH_LONG).show();
        }
/*        appWidgetId = intent.getIntExtra(AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID)*/

    }

    @Override
    public void onCreate() {

    }


    @Override
    public void onDataSetChanged() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public int getCount() {

        try {
            return collection.getGroupCount();
        } catch (NullPointerException e) {
            return 0;
        }
    }

    @Override
    public RemoteViews getViewAt(int position) {

        RemoteViews row = new RemoteViews(context.getPackageName(), R.layout.entry_group);

        try {
            row.setTextViewText(R.id.tv_group_title, collection.getExerciseGroup(position).getGroupName());
            row.setTextViewText(R.id.tv_exercise_count, ""+collection.getExerciseGroup(position).getExercises().size());

            //fill in the intent
            Intent intent = new Intent();
            Bundle extras = new Bundle();
            extras.putInt(ExerciseProvider.EXTRA_LIST, position);
            intent.putExtras(extras);
            row.setOnClickFillInIntent(R.id.tv_group_title, intent);

            return row;

        } catch (NullPointerException e) {
            Log.e(TAG, "getViewAt: Error", e);
            row.setTextViewText(R.id.tv_group_title, "-1");
            row.setTextViewText(R.id.tv_exercise_count, "-1");
            return row;

        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
