package com.gorillamoa.routines.providers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.gorillamoa.routines.R;
import com.gorillamoa.routines.activities.MainMenuActivity;
import com.gorillamoa.routines.activities.ExerciseDetailActivity;
import com.gorillamoa.routines.activities.ResultFetchingActivity;
import com.gorillamoa.routines.data.AppearanceSettings;
import com.gorillamoa.routines.data.DataPointCollection;
import com.gorillamoa.routines.data.ExerciseProgress;
import com.gorillamoa.routines.fragments.ExerciseDetailFragment;
import com.gorillamoa.routines.managers.ExerciseManager;
import com.gorillamoa.routines.app.App;
import com.gorillamoa.routines.data.ExerciseCollection;
import com.gorillamoa.routines.network.SynchronizeSpreadSheetTask;
import com.gorillamoa.routines.services.ListService;

import java.io.IOException;

/**
 * Created by alvaregd on 10/10/15.
 * Handles how the widget appearance and data and actions
 */
public class ExerciseProvider extends AppWidgetProvider {

    private static final String TAG = ExerciseProvider.class.getName();

    private static final boolean d_getNextStateBasedOnData = false;
    private static final boolean d_onReceive = true;
    private static final boolean d_getPendingIntentActivity = false;
    private static final boolean d_setWidgetUIContent = true;
    private static final boolean d_LoadContent = false;
    private static final boolean d_decrementValue = false;
    private static final boolean d_RefreshContent = false;
    private static final boolean d_incrementValue = false;
    private static final boolean d_createDynamicGradient = false;
    private static final boolean d_checkAndUpdateTimer = true;

    private static final String DONE_CLICKED = "ACTION_CLICK";
    private static final String REFRESH_CLICKED = "ACTION_REFRESH";
    private static final String EDIT_REP_CLICKED = "ACTION_REP";
    private static final String EDIT_WEIGHT_CLICKED = "ACTION_WEIGHT";
    private static final String EDIT_CANCEL_CLICKED = "ACTION_CANCEL";
    private static final String EDIT_ACCEPT_CLICKED_ALL = "ACTION_ACCEPT_ONE";
    private static final String EDIT_ACCEPT_CLICKED_ONE = "ACTION_ACCEPT_ALL";
    private static final String EDIT_INCREMENT_CLICKED = "ACTION_INCREMENTS";
    private static final String EDIT_DECREMENT_CLICKED = "ACTION_DECREMENT";

    private static final String NOTE_CLICKED = "NOTE_CLICKED";
    private static final String START_CLICKED = "ACTION_START";
    private static final String PICK_CANCEL_CLICKED = "PCC";
    private static final String LIST_CLICKED = "LCLICK";
    private static final String TIMER_UPDATE = "TIMER_UPDATE";
    private static final String NOTE_REFRESH = "NOTE_FRESH";
    private static final String FINISH_MENU_CLICK = "FMC";
    private static final String EXERCISE_NEXT = "NEXTEXER";
    private static final String EXERCISE_PREVIOUS = "PREVIOUSEXER";
    private static final String NOTE_QUIT_CLICKED = "NOTEQUIT";

    private final static long[] pattern = { 0, 100, 500, 100, 500, 100, 500}; //vibration pattern
    private final static long[] done_parrent = {0, 100,400};

    public final static String EXTRA_LIST = "LINDEX";

    private final static int DAY_ACTIVITY = 0;
    private final static int RESULTFETCH_ACTIVITY = 1;
    private final static int EXERCISE_VIEW_ACTIVITY = 2;

    private enum State {
        NoActivity,//default page to show when the user is not on an exercise group, shows stats and start button
        Standard,  //The standard page to show while on the exercise, shows current exercise progress
        Finished,  //the finish page, shows current exercise stats
        Start,     //the page containing a list of exercise groups
        EditWeight,//edit the weight
        EditReps,  //edit the number of  reps
        Error,     //error page
        Note,      // note page, show current exercise instructions
        PickActivity, //
    }


    private App app;
    private static State state;
    private static ExerciseCollection collection;
    private static DataPointCollection pointCollection;
    private static Intent widgetIntent;
    private static AppearanceSettings widgetAppearanceSettings;
    static String[] data;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager,  int[] appWidgetIds) {
        RefreshContent(context, appWidgetManager);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);

        if(d_onReceive)Log.d(TAG, "onReceive: enter:" + intent.getAction());
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        widgetIntent = intent;

        if (DONE_CLICKED.equals(intent.getAction())) {

            LoadContent(context);
            ExerciseManager.calculateNextExerciseValues(collection,context);
            data = ExerciseManager.getExerciseValues(collection, context);
            state = getNextStateBasedOnData(data);
            App.saveCollection(collection, app.getPreferences(),app.getGson());
            updateUI(context, appWidgetManager, data, checkAndUpdateTimer(context));

            Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
            v.vibrate(done_parrent , -1);

        }else if (REFRESH_CLICKED.equals(intent.getAction())) {

            RefreshContent(context, appWidgetManager);
            synchronize(context);

        }else if (EDIT_REP_CLICKED.equals(intent.getAction())) {

            state = State.EditReps;
            prepareForEditing(context, appWidgetManager);

        }else if (EDIT_WEIGHT_CLICKED.equals(intent.getAction())) {

            state = State.EditWeight;
            prepareForEditing(context, appWidgetManager);

        }else if (EDIT_ACCEPT_CLICKED_ONE.equals(intent.getAction())) {

            if(d_onReceive)Log.d(TAG, "onReceive: Edit Accept Clicked");
            RefreshContent(context, appWidgetManager);

        }else if (EDIT_ACCEPT_CLICKED_ALL.equals(intent.getAction())) {

            if(d_onReceive)Log.d(TAG, "onReceive: Edit Accept Clicked");
            applyChangeToExerciseDetail(context, appWidgetManager);

        }else if (EDIT_CANCEL_CLICKED.equals(intent.getAction())) {

            if(d_onReceive)Log.d(TAG, "onReceive: Edit Cancel Clicked");
            cancelEdit(context, appWidgetManager);

        }else if (START_CLICKED.equals(intent.getAction())) {
            if(d_onReceive)Log.d(TAG, "onReceive: Start Clicked");

            state = State.PickActivity;
            LoadContent(context);
            updateUI(context,appWidgetManager,null,null);

        } else if (PICK_CANCEL_CLICKED.equals(intent.getAction())) {

            if(d_onReceive ) Log.d(TAG, "onReceive: Pick Cancel Clicked!");
            state = State.NoActivity;
            RefreshContent(context, appWidgetManager);

        } else if (LIST_CLICKED.equals(intent.getAction())) {

            Log.d(TAG, "onReceive: Clicked Item: " + intent.getIntExtra(EXTRA_LIST,-1));
            LoadContent(context);
            ExerciseManager.startActivity(collection,intent.getIntExtra(EXTRA_LIST,-1));
            data = ExerciseManager.getExerciseValues(collection, context);
            state = State.Standard;
            updateUI(context,appWidgetManager,data,null);


        } else if (NOTE_CLICKED.equals(intent.getAction())) {

            LoadContent(context);
            state = State.Note;
            updateUI(context, appWidgetManager, null, null);

        } else if (EDIT_INCREMENT_CLICKED.equals(intent.getAction())) {
            incrementValue(context, appWidgetManager);

        } else if (EDIT_DECREMENT_CLICKED.equals(intent.getAction())) {
            decrementValue(context, appWidgetManager);
        }

        //debug
        else if (NOTE_REFRESH.equals(intent.getAction())) {

            LoadContent(context);
            state = State.Note;
            updateUI(context, appWidgetManager, null, null);

        } else if (NOTE_QUIT_CLICKED.equals(intent.getAction())) {

            LoadContent(context);
            ExerciseManager.quitRoutine(collection);
            App.saveCollection(collection, app.getPreferences(), app.getGson());
            data = ExerciseManager.getExerciseValues(collection,context);
            state = getNextStateBasedOnData(data);
            updateUI(context, appWidgetManager, data, null);

        } else if (TIMER_UPDATE.equals(intent.getAction())) {

            if (state == State.Standard) {

                Log.d(TAG, "TIMER_UPDATE()");
                LoadContent(context);
                data = ExerciseManager.getExerciseValues(collection, context);
                App.saveCollection(collection, app.getPreferences(), app.getGson());
                state = getNextStateBasedOnData(data);
                updateUI(context, appWidgetManager, data, checkAndUpdateTimer(context));
            } else {
                checkAndUpdateTimer(context);
            }
        } else if (FINISH_MENU_CLICK.equals(intent.getAction())) {

            LoadContent(context);
            ExerciseManager.endActivity(collection);
            App.saveCollection(collection, app.getPreferences(), app.getGson());
            state = State.NoActivity;
            updateUI(context, appWidgetManager, data, null);

        } else if (EXERCISE_NEXT.equals(intent.getAction())) {

            LoadContent(context);
            ExerciseManager.nextExercise(collection);
            App.saveCollection(collection, app.getPreferences(), app.getGson());
            data = ExerciseManager.getExerciseValues(collection, context);
            state = getNextStateBasedOnData(data);
            updateUI(context, appWidgetManager, data, null);

        } else if (EXERCISE_PREVIOUS.equals(intent.getAction())) {

            LoadContent(context);
            ExerciseManager.previousExercise(collection);
            App.saveCollection(collection, app.getPreferences(), app.getGson());
            data = ExerciseManager.getExerciseValues(collection, context);
            state = getNextStateBasedOnData(data);
            updateUI(context, appWidgetManager, data, null);
        }
    }

    public static PendingIntent getPendingIntentSelf(Context context, String action) {

        Intent intent = new Intent(context, ExerciseProvider.class);
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

    }

    public static  PendingIntent getPendingIntentActivity(Context context,int activity) {

        Intent intent;
        switch (activity) {
            case DAY_ACTIVITY: intent = new Intent(context, MainMenuActivity.class);break;
            case RESULTFETCH_ACTIVITY : intent = new Intent(context, ResultFetchingActivity.class);break;
            case EXERCISE_VIEW_ACTIVITY: {
                intent = new Intent(context, ExerciseDetailActivity.class);
                intent.putExtra(ExerciseDetailFragment.ARG_EXERCISE_ID, ""+ String.valueOf(collection.getProgress().getCurrentExercise()));
                intent.putExtra(ExerciseDetailFragment.ARG_GROUP_ID,    ""+ String.valueOf(collection.getProgress().getCurrentGoup()));
                break;
            }
            default: intent = new Intent(context, MainMenuActivity.class); break;
        }
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    public static RemoteViews setWidgetUIContent(String[] data, Context context, String time, int widgetId) {
        RemoteViews remoteViews;
        switch (state) {
            case NoActivity:

                remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_noexercise);
                remoteViews.setTextViewText(R.id.available_title, "Page Under Construction");
                remoteViews.setOnClickPendingIntent(R.id.start_label, getPendingIntentSelf(context, START_CLICKED));
                remoteViews.setOnClickPendingIntent(R.id.rl_background_start, getPendingIntentSelf(context, REFRESH_CLICKED));
                remoteViews.setOnClickPendingIntent(R.id.menu_label, getPendingIntentActivity(context,DAY_ACTIVITY));
                remoteViews.setImageViewBitmap(R.id.background, createDynamicGradient());

                break;
            case PickActivity:

                if(d_setWidgetUIContent) Log.d(TAG, "setWidgetUIContent: Pick Activity");
                remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_pick_activity);
                remoteViews.setOnClickPendingIntent(R.id.note_layout, getPendingIntentSelf(context, PICK_CANCEL_CLICKED));
                Intent svcIntent = new Intent(context, ListService.class);
                svcIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
                svcIntent.setData(Uri.parse((svcIntent.toUri(Intent.URI_INTENT_SCHEME))));
                remoteViews.setRemoteAdapter(R.id.exercise_list,svcIntent);
                remoteViews.setPendingIntentTemplate(R.id.exercise_list, getPendingIntentSelf(context,LIST_CLICKED));
                remoteViews.setImageViewBitmap(R.id.background, createDynamicGradient());

                break;
            case Standard:

                remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_standard);
                remoteViews.setTextViewText(R.id.set_number, data[ExerciseManager.SET]);
                remoteViews.setTextViewText(R.id.rep_number, data[ExerciseManager.REP]);
                remoteViews.setTextViewText(R.id.weight_number, data[ExerciseManager.WEIGHT]);
                remoteViews.setTextViewText(R.id.exercise_name, data[ExerciseManager.NAME]);
                remoteViews.setTextViewText(R.id.progress_percent, data[ExerciseManager.PROGRESS]);
                remoteViews.setTextViewText(R.id.tv_rest_text, time);

                Log.d(TAG, "Time:" + time);

                remoteViews.setOnClickPendingIntent(R.id.exercise_name, getPendingIntentActivity(context, EXERCISE_VIEW_ACTIVITY));
                remoteViews.setOnClickPendingIntent(R.id.rep_number, getPendingIntentSelf(context, EDIT_REP_CLICKED));
                remoteViews.setOnClickPendingIntent(R.id.weight_number, getPendingIntentSelf(context, EDIT_WEIGHT_CLICKED));
                remoteViews.setOnClickPendingIntent(R.id.done, getPendingIntentSelf(context, DONE_CLICKED));
//                remoteViews.setOnClickPendingIntent(R.id.refresh_standard, getPendingIntentSelf(context, REFRESH_CLICKED));
                remoteViews.setOnClickPendingIntent(R.id.button_note, getPendingIntentSelf(context, NOTE_CLICKED));
                remoteViews.setOnClickPendingIntent(R.id.button_previous_exercise, getPendingIntentSelf(context, EXERCISE_PREVIOUS));
                remoteViews.setOnClickPendingIntent(R.id.button_next_exercise, getPendingIntentSelf(context, EXERCISE_NEXT));

                remoteViews.setViewVisibility(R.id.button_previous_exercise, ExerciseManager.getPreviousViewVisibility(collection));
                remoteViews.setViewVisibility(R.id.button_next_exercise, ExerciseManager.getNextViewVisibility(collection));
                int done = ExerciseManager.getDoneViewVisibility(collection);
                remoteViews.setViewVisibility(R.id.done,  done);
                if (done == View.INVISIBLE) {
                    remoteViews.setInt(R.id.exercise_name, "setPaintFlags", Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
                    remoteViews.setInt(R.id.set_number, "setPaintFlags", Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
                    remoteViews.setInt(R.id.rep_number, "setPaintFlags", Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
                    remoteViews.setInt(R.id.weight_number, "setPaintFlags", Paint.STRIKE_THRU_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);
                }else{
                    remoteViews.setInt(R.id.exercise_name, "setPaintFlags", Paint.ANTI_ALIAS_FLAG);
                    remoteViews.setInt(R.id.set_number, "setPaintFlags", Paint.ANTI_ALIAS_FLAG);
                    remoteViews.setInt(R.id.rep_number, "setPaintFlags", Paint.ANTI_ALIAS_FLAG);
                    remoteViews.setInt(R.id.weight_number, "setPaintFlags", Paint.ANTI_ALIAS_FLAG);
                }
                remoteViews.setImageViewBitmap(R.id.background, createDynamicGradient());
                break;

            case Finished:

                remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_done);
                remoteViews.setTextViewText(R.id.tv_quote, ExerciseManager.getQuote(context));
                remoteViews.setTextViewText(R.id.tv_time_spent, data[ExerciseManager.TIME_ELAPSED]);
                remoteViews.setOnClickPendingIntent(R.id.refresh_done, getPendingIntentSelf(context, REFRESH_CLICKED));
                remoteViews.setOnClickPendingIntent(R.id.tv_done_menu,getPendingIntentSelf(context, FINISH_MENU_CLICK));
                remoteViews.setImageViewBitmap(R.id.background, createDynamicGradient());

                break;
            case EditReps:

                if(d_setWidgetUIContent)Log.d(TAG, "setWidgetUIContent: Edit Reps");
                remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_edit_page);
                remoteViews.setTextViewText(R.id.edit_title, context.getResources().getText(R.string.title_change_reps));
                remoteViews.setTextViewText(R.id.value , data[ExerciseManager.REP]);
                remoteViews.setOnClickPendingIntent(R.id.tv_accept_all, getPendingIntentSelf(context, EDIT_ACCEPT_CLICKED_ALL));
                remoteViews.setOnClickPendingIntent(R.id.tv_accept_one, getPendingIntentSelf(context, EDIT_ACCEPT_CLICKED_ONE));
                remoteViews.setOnClickPendingIntent(R.id.button_cancel, getPendingIntentSelf(context, EDIT_CANCEL_CLICKED));
                remoteViews.setOnClickPendingIntent(R.id.ib_increment,getPendingIntentSelf(context,EDIT_INCREMENT_CLICKED));
                remoteViews.setOnClickPendingIntent(R.id.ib_decrement,getPendingIntentSelf(context,EDIT_DECREMENT_CLICKED));
                remoteViews.setImageViewBitmap(R.id.background, createDynamicGradient());

                break;
            case EditWeight:

                if(d_setWidgetUIContent)Log.d(TAG, "setWidgetUIContent: Edit Weights");
                remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_edit_page);
                remoteViews.setTextViewText(R.id.edit_title, context.getResources().getText(R.string.title_change_Weight));
                remoteViews.setTextViewText(R.id.value , data[ExerciseManager.WEIGHT]);
                remoteViews.setOnClickPendingIntent(R.id.tv_accept_all, getPendingIntentSelf(context, EDIT_ACCEPT_CLICKED_ALL));
                remoteViews.setOnClickPendingIntent(R.id.tv_accept_one, getPendingIntentSelf(context, EDIT_ACCEPT_CLICKED_ONE));
                remoteViews.setOnClickPendingIntent(R.id.button_cancel, getPendingIntentSelf(context, EDIT_CANCEL_CLICKED));
                remoteViews.setOnClickPendingIntent(R.id.ib_increment, getPendingIntentSelf(context, EDIT_INCREMENT_CLICKED));
                remoteViews.setOnClickPendingIntent(R.id.ib_decrement,getPendingIntentSelf(context,EDIT_DECREMENT_CLICKED));
                remoteViews.setImageViewBitmap(R.id.background, createDynamicGradient());
                break;

            case Start:

                remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_exercise_available);
                remoteViews.setTextViewText(R.id.available_title, "There are " + data[ExerciseManager.REP] + " exercise(s) available!");
                remoteViews.setOnClickPendingIntent(R.id.start_label, getPendingIntentSelf(context, START_CLICKED));
                remoteViews.setOnClickPendingIntent(R.id.rl_background_start, getPendingIntentSelf(context, REFRESH_CLICKED));
                remoteViews.setOnClickPendingIntent(R.id.menu_label, getPendingIntentActivity(context,DAY_ACTIVITY));
                remoteViews.setImageViewBitmap(R.id.background, createDynamicGradient());
                break;

            case Note:

                remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_note_view);
                remoteViews.setTextViewText(R.id.tv_note, ExerciseManager.getNote(collection));
                remoteViews.setOnClickPendingIntent(R.id.note_layout, getPendingIntentSelf(context, EDIT_CANCEL_CLICKED));
                remoteViews.setOnClickPendingIntent(R.id.note_quit, getPendingIntentSelf(context, NOTE_QUIT_CLICKED));
                remoteViews.setImageViewBitmap(R.id.background, createDynamicGradient());

//              remoteViews.setOnClickPendingIntent(R.id.refresh_standard, getPendingIntentSelf(context, NOTE_REFRESH));
//              remoteViews.setOnClickPendingIntent(R.id.inc, getPendingIntentSelf(context, NOTE_INC));
//              remoteViews.setOnClickPendingIntent(R.id.dec, getPendingIntentSelf(context, NOTE_DEC));
                break;
            default:
                if(d_setWidgetUIContent)Log.d(TAG, "setWidgetUIContent: Defaulted");
                remoteViews = null;
                break;
        }
        return remoteViews;
    }

        //fetch our data
    private void LoadContent(Context context) {

        if (app == null) {
            app = ((App)context.getApplicationContext());
        }

        if (collection == null) {
            if (d_LoadContent) Log.d(TAG, "onReceive: Null collection");
            collection = App.LoadCollection(app.getPreferences(), app.getGson());
            if (collection == null) {
                Toast.makeText(context, "Unable to Load Data!", Toast.LENGTH_LONG).show();
            }
        }
        if (widgetAppearanceSettings == null) {
            widgetAppearanceSettings = ((App) context.getApplicationContext()).getWidgetAppearanceSettings();
        }

        if (pointCollection == null) {
            pointCollection = ((App) context.getApplicationContext()).getDataPointCollection();
        }
    }

    private void RefreshContent(Context context, AppWidgetManager appWidgetManager){

        if(d_RefreshContent)Log.d(TAG, "RefreshContent: Enter");
        LoadContent(context);
        String[] data = ExerciseManager.getExerciseValues( collection,context);
        App.saveCollection(collection,app.getPreferences(),app.getGson());
        state = getNextStateBasedOnData(data);
        updateUI(context, appWidgetManager, data,null);
    }

    private void cancelEdit(Context context, AppWidgetManager appWidgetManager){

        LoadContent(context);
        ExerciseProgress progress = collection.getProgress();
        progress.setEditRepValue(-1);
        progress.setEditWeightValue(-1);
        App.saveCollection(collection, app.getPreferences(),app.getGson());
        RefreshContent(context, appWidgetManager);

    }

    private void prepareForEditing(Context context, AppWidgetManager appWidgetManager) {

        Log.d(TAG, "RefreshContent: Enter");
        LoadContent(context);
        String[] data = ExerciseManager.getExerciseValues(collection,context);

        switch (state) {
            case EditReps:
                collection.getProgress().setEditRepValue(Integer.valueOf(data[ExerciseManager.REP]));
                collection.getProgress().setEditWeightValue(-1);
                break;
            case EditWeight:
                collection.getProgress().setEditWeightValue(Float.valueOf(data[ExerciseManager.WEIGHT]));
                collection.getProgress().setEditRepValue(-1);
                break;
        }
        App.saveCollection(collection,app.getPreferences(),app.getGson());
        updateUI(context, appWidgetManager, data, null);
    }

    private void incrementValue(Context context, AppWidgetManager appWidgetManager){

        if(d_incrementValue) Log.e(TAG, "incrementValue() ");
        Log.d(TAG, "incrementValue: Enter");
        LoadContent(context);
        String[] data = new String[5];

        if(collection.getProgress().getEditRepValue() == -1) {

            //we are editing the weight value
            state = State.EditWeight;
            float weight = collection.getProgress().getEditWeightValue() + 2.5f;
            if(weight < 0) weight = 0f;
            collection.getProgress().setEditWeightValue(weight);
            data[ExerciseManager.WEIGHT] = String.valueOf(weight);

        }else if (collection.getProgress().getEditWeightValue() == -1){
            //we are editing the rep value

            state = State.EditReps;
            int rep = collection.getProgress().getEditRepValue() + 1;
            if(rep <0) rep = 0;
            collection.getProgress().setEditRepValue(rep);
            data[ExerciseManager.REP] = String.valueOf(rep);
        }

        App.saveCollection(collection, app.getPreferences(),app.getGson());
        updateUI(context, appWidgetManager, data,null);
    }

    private void decrementValue(Context context, AppWidgetManager appWidgetManager){

        if(d_decrementValue) Log.d(TAG, "decrementValue: Enter");
        LoadContent(context);
        String[] data = new String[5];

        if(collection.getProgress().getEditRepValue() == -1) {

            //we are editing the weight value
            state = State.EditWeight;
            float weight = collection.getProgress().getEditWeightValue() - 2.5f;
            if(weight < 0) weight = 0f;
            collection.getProgress().setEditWeightValue(weight);
            data[ExerciseManager.WEIGHT] = String.valueOf(weight);

        }else if (collection.getProgress().getEditWeightValue() == -1) {

            //we are editing the rep value
            state = State.EditReps;
            int rep = collection.getProgress().getEditRepValue() - 1;
            if (rep < 0) rep = 0;
            collection.getProgress().setEditRepValue(rep);
            data[ExerciseManager.REP] = String.valueOf(rep);

        }

        App.saveCollection(collection, app.getPreferences(),app.getGson());
        updateUI(context, appWidgetManager, data,null);
    }

    private void applyChangeToExerciseDetail(Context context, AppWidgetManager appWidgetManager){

        LoadContent(context);
        ExerciseManager.changeValuesOfExericse(context,collection);
        App.saveCollection(collection,app.getPreferences(),app.getGson());
        RefreshContent(context, appWidgetManager);
    }

    private void updateUI(Context context, AppWidgetManager appWidgetManager, String[] data, String time){

        ComponentName thisWidget = new ComponentName(context, ExerciseProvider.class);
        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);

        //update all widget instances
        for (int widgetId : allWidgetIds) {

            //create our widget UI
            RemoteViews remoteViews = setWidgetUIContent(data, context,time, widgetId);
            // Prepare for the next button pressRegister an onClickListener
            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }
    }

    /**
     * Get the next widget state based on the data calculated with current state
     * @param data contains s
     * @return
     */
    private State getNextStateBasedOnData(String[] data) {
        if(d_getNextStateBasedOnData) Log.e(TAG, "getNextStateBasedOnData() ");

        switch (data[ExerciseManager.SET]) {
            case ExerciseManager.NO_ACTIVE_GROUP:
                return State.NoActivity;
            case ExerciseManager.ALL_EXERCISES_DONE:
                return State.Finished;
            default:
                return State.Standard;
        }
    }

    private static Bitmap createDynamicGradient() {
        if (d_createDynamicGradient) Log.e(TAG, "createDynamicGradient() Enter");
        int width;
        int height;
        int bottom, right;
        if (widgetIntent != null) {
            if (widgetIntent.getSourceBounds() != null) {
                width = widgetIntent.getSourceBounds().width();
                height = widgetIntent.getSourceBounds().height();
                bottom = widgetIntent.getSourceBounds().bottom;
                right = widgetIntent.getSourceBounds().right;
            } else {

                width = 100;
                height = 100;
                bottom = 100;
                right = 100;
            }

        } else {
            bottom = 100;
            right = 100;
            width = 100;
            height = 100;
        }
        try {

            LinearGradient gradient = new LinearGradient(
                    0, 0, 0, 400,
                    widgetAppearanceSettings.homeWidgetBackgroundColour,
                    widgetAppearanceSettings.homeWidgetBackgroundColour,
                    Shader.TileMode.CLAMP);
            Paint p = new Paint();
            p.setDither(true);
            p.setShader(gradient);

            //define our rectangle
            RectF rectangle = new RectF(0, 0, right, bottom);
            final float roundDp = 5;

            Bitmap bitmap = Bitmap.createBitmap((width), (height), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            canvas.drawRoundRect(rectangle, roundDp, roundDp, p);

            return bitmap;
        } catch (NullPointerException e) {

            Bitmap bitmap = Bitmap.createBitmap((width), (height), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            canvas.drawARGB(0,0,0,0);
            return bitmap;
        }
    }

    private void resetTimer(int seconds, final Context context) {

        PendingIntent pendingIntent = getPendingIntentSelf(context, TIMER_UPDATE);
        final AlarmManager m = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        m.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * seconds, pendingIntent);

    }

    public String checkAndUpdateTimer(Context context){

        try{
            String time;
            int timeRemaining = collection.getProgress().getTimeRemaining();
            time = "Rest:"+ timeRemaining+ "s";

            if(d_checkAndUpdateTimer) Log.e(TAG, "checkAndUpdateTimer() " + time);

            if (timeRemaining > 0 && collection.getProgress().isActive()) {
                resetTimer(1, context); //1 second from now
                collection.getProgress().setTimeRemaining(timeRemaining - 1);

                if (timeRemaining == 1) {

                    //vibrate
                    Vibrator v = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
                    v.vibrate(pattern , -1);

                    //play sound
                    Uri defaultRingtoneUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

                    MediaPlayer mediaPlayer = new MediaPlayer();

                    try {
                        mediaPlayer.setDataSource(context, defaultRingtoneUri);
                        mediaPlayer.setAudioStreamType(AudioManager.STREAM_NOTIFICATION);
                        mediaPlayer.prepare();
                        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

                            @Override
                            public void onCompletion(MediaPlayer mp)
                            {
                                mp.release();
                            }
                        });
                        mediaPlayer.start();
                    } catch (IllegalArgumentException e) {
                        e.printStackTrace();
                    } catch (SecurityException e) {
                        e.printStackTrace();
                    } catch (IllegalStateException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }else {
                time = "";
            }
            return time;
        }catch (NullPointerException e){
            return "";
        }
    }

    /** attempt to save local data remotely **/
    private void synchronize(Context context) {

        //make fake data
      /*  DataPointCollection collection = new DataPointCollection();
        collection.init();

        for (int i = 0; i < 7; i++) {
            Datapoint data = new Datapoint();
            data.reps = i;
            data.weight = i;
            data.name = "Exercise " + i ;
            data.day = i + 1 ;
            data.date = "2015/12/16";
            collection.getPoints().add(data);
        }

        SynchronizeSpreadSheetTask task = new SynchronizeSpreadSheetTask(context);
        task.execute(gson.toJson(collection));*/

        if (pointCollection != null) {
            if (pointCollection.getPoints().size() > 0) {
                SynchronizeSpreadSheetTask task = new SynchronizeSpreadSheetTask(context);
                task.setCollection(pointCollection);
                task.synchronize(app.getGson());

            }
        }
    }
}
