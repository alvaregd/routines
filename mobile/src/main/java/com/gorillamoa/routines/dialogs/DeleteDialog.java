package com.gorillamoa.routines.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.gorillamoa.routines.R;

/**
 * Created by alvaregd on 02/02/16.
 * Dialog that pops up when long pressing an item on the list
 */
public class DeleteDialog extends DialogFragment{

    public interface DeleteDialogListener{
        void DeleteClicked();
        String getSeletedName();
    }

    DeleteDialogListener listener;
    private String name;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = (DeleteDialogListener) activity;
            name = listener.getSeletedName();
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View rootView;
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        rootView = inflater.inflate(R.layout.fragment_delete_dialog, null);

        //set the title
        TextView title;

        Resources res =getResources();
        name = res.getString(R.string.delete_title_prefix) + name + res.getString(R.string.delete_title_affix);
        title = (TextView)rootView.findViewById(R.id.dialog_title);
        title.setText(name);

        builder.setView(rootView)
                .setPositiveButton(R.string.dialog_create, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.DeleteClicked();
                    }
                })

                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DeleteDialog.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }
}
