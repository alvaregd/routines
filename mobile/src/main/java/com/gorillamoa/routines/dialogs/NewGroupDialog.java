package com.gorillamoa.routines.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import com.gorillamoa.routines.R;

/**
 * Created by alvaregd on 01/02/16.
 */
public class NewGroupDialog extends DialogFragment {

    public interface NewGroupDialogListener {
        void CreateClicked(String name);
    }

    private View rootView;
    private EditText etGroupName;
    NewGroupDialogListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = (NewGroupDialogListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        rootView = inflater.inflate(R.layout.fragment_new_group, null);
        etGroupName = (EditText) rootView.findViewById(R.id.et_group_new);

        builder.setView(rootView)

                .setPositiveButton(R.string.dialog_create, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.CreateClicked(etGroupName.getText().toString());
                    }
                })

                .setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        NewGroupDialog.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }
}
