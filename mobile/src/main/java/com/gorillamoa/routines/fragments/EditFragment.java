package com.gorillamoa.routines.fragments;

/**
 * Created by alvaregd on 04/02/16.
 */

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.gorillamoa.routines.R;

/**
 * A fragment containing the Generate Routine Sheet stuff .
 */
public class EditFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    public EditFragment() {
    }

    public interface EditFragmentCallback{
        void onDoneClicked();
    }

    private EditFragmentCallback listener;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static EditFragment newInstance(int sectionNumber) {
        EditFragment fragment = new EditFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_edit, container, false);

        Button editDone = (Button) rootView.findViewById(R.id.b_done);
        editDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onDoneClicked();
            }
        });

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.listener = (EditFragmentCallback) context;
    }
}
