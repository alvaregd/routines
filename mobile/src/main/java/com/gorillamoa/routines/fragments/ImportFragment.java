package com.gorillamoa.routines.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.gorillamoa.routines.R;

/**
 * A fragment containing the Generate Routine Sheet stuff .
 */
public class ImportFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    public ImportFragment() {
    }

    public interface ImportFragmentCallback{
        void onImportClicked();
    }

    ImportFragmentCallback listener;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ImportFragment newInstance(int sectionNumber) {
        ImportFragment fragment = new ImportFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_import, container, false);

        Button importStuff =(Button) rootView.findViewById(R.id.b_import);
        importStuff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                listener.onImportClicked();
            }
        });
        return rootView;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (ImportFragmentCallback)context;
    }
}