package com.gorillamoa.routines.fragments;

/**
 * Created by alvaregd on 04/02/16.
 */


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.gorillamoa.routines.R;
import com.gorillamoa.routines.dialogs.GenerateDialog;

/**
 * A fragment containing the Generate Routine Sheet stuff .
 */
public class GenerateRoutineFragment extends Fragment{


    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    public GenerateRoutineFragment() {
    }

    public interface GenerateRoutineFragmentCallback{
        void skipClicked();
    }

    GenerateRoutineFragmentCallback listener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listener = (GenerateRoutineFragmentCallback)context;
    }

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static GenerateRoutineFragment newInstance(int sectionNumber) {
        GenerateRoutineFragment fragment = new GenerateRoutineFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_generate_routine, container, false);
//        TextView textView = (TextView) rootView.findViewById(R.id.section_label);
//        textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));

        Button skip = (Button)rootView.findViewById(R.id.b_skip);
        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.skipClicked();
            }
        });

        Button generate = (Button)rootView.findViewById(R.id.b_generate);
        generate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment dialog = new GenerateDialog();
                dialog.show(getFragmentManager(), "PromptGenerate");
            }
        });

        return rootView;
    }

}

