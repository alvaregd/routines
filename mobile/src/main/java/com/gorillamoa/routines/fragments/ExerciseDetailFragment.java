package com.gorillamoa.routines.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.gorillamoa.routines.R;
import com.gorillamoa.routines.activities.ExerciseDetailActivity;
import com.gorillamoa.routines.activities.ExerciseListActivity;
import com.gorillamoa.routines.data.Exercise;
import com.gorillamoa.routines.app.App;
import com.gorillamoa.routines.data.ExerciseCollection;
import com.gorillamoa.routines.data.ExerciseGroup;
import com.gorillamoa.routines.data.FetchedDataPoint;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import lecho.lib.hellocharts.gesture.ContainerScrollType;
import lecho.lib.hellocharts.listener.ColumnChartOnValueSelectListener;
import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.Column;
import lecho.lib.hellocharts.model.ColumnChartData;
import lecho.lib.hellocharts.model.SubcolumnValue;
import lecho.lib.hellocharts.util.ChartUtils;
import lecho.lib.hellocharts.view.ColumnChartView;

/**
 * A fragment representing a single Exercise detail screen.
 * This fragment is either contained in a {@link ExerciseListActivity}
 * in two-pane mode (on tablets) or a {@link ExerciseDetailActivity}
 * on handsets.
 */
public class ExerciseDetailFragment extends Fragment implements View.OnClickListener
{
    private static final String TAG = ExerciseDetailFragment.class.getName();
    public static final boolean d_onCreate= false;
    public static final boolean d_parseData = false;
    public static final boolean d_onCreateView = false;

    public interface NetworkRequestListener{
        void onDataRequested(String routine, String exercise);
    }

    private ExerciseCollection collection;
    App app;

    private ExerciseGroup group;
    private Exercise exercise;

    CollapsingToolbarLayout appBarLayout;

    private int selectedGroup;
    private int selectedExercise;

    /** chart stuff */
    ColumnChartView volumnView;
    ColumnChartView weightView;
    ColumnChartView repView;

    private ColumnChartData volumeData;
    private ColumnChartData weightData;
    private ColumnChartData repData;

    private TextView recentVolume, recentWeight, recentRep;  //Textview that appear in the "Most recent" section
    private TextView volumeHighest, volumeRcent, volumeGrowth; //textview that appear in the volume section
    private TextView labelVolOne, labelVolTwo, labelVolThree;
    private TextView weightHighest, weightRecent, weightGrowth; //textviews that appear in the weight section
    private TextView labelWeiOne, labelWeiTwo, labelWeiThree;
    private TextView repRecent;
    private TextView totalSession, totalWeight, totalReps, totalSets;

    Calendar calendar;

    View rootView;

    List<FetchedDataPoint> points;
    /*** The fragment argument representing the item ID that this fragment represents.*/
    public static final String ARG_EXERCISE_ID = "item_id";
    public static final String ARG_GROUP_ID = "group_id";


    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public static Fragment newInstance(){
        ExerciseDetailFragment fragment = new ExerciseDetailFragment();
        return fragment;
    }

    public ExerciseDetailFragment() {}



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(d_onCreate) Log.e(TAG, "onCreate() Enter");

        loadCollection();
        Activity activity = this.getActivity();

        //Check if we are making a new exercise or if we are loading/editing another one
        if (getArguments().containsKey(ARG_GROUP_ID) && getArguments().containsKey(ARG_EXERCISE_ID)) {

            if (getArguments().getString(ARG_GROUP_ID) != null && getArguments().getString(ARG_EXERCISE_ID) != null) {

                selectedGroup = Integer.parseInt(getArguments().getString(ARG_GROUP_ID));
                selectedExercise = Integer.parseInt(getArguments().getString(ARG_EXERCISE_ID));
                Log.d(TAG, "group id: " + selectedGroup);
                Log.d(TAG, "exercise id: " + selectedExercise);
                group = collection.getExerciseGroup(selectedGroup);
                exercise = group.getExercises().get(selectedExercise);
            }

            if (exercise != null) {
                if(d_onCreate) Log.e(TAG, "onCreate() " + "Exercise OK " + selectedExercise);
                appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
                if (appBarLayout != null) {
                    appBarLayout.setTitle(exercise.name);
                }
            }else{
                Log.d(TAG, "onCreate: Problem Loading exercise");
            }
        }else{
            Log.d(TAG, "onCreate: One or both IDs is missing");
        }
    }

    private void loadCollection() {
        app = ((App)getActivity().getApplicationContext());
        collection = App.LoadCollection(app.getPreferences(),app.getGson());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(d_onCreateView) Log.e(TAG, "onCreateView() Enter");
        if (rootView == null) {

            calendar = Calendar.getInstance();

            rootView = inflater.inflate(R.layout.exercise_detail, container, false);

            volumnView = (ColumnChartView) rootView.findViewById(R.id.volumeChart);
            volumnView.setInteractive(true);
            volumnView.setZoomEnabled(false);
            volumnView.setContainerScrollEnabled(false, ContainerScrollType.VERTICAL);
            volumnView.setContainerScrollEnabled(false, ContainerScrollType.HORIZONTAL);
            volumnView.setValueSelectionEnabled(true);
            volumnView.setOnValueTouchListener(new ColumnChartOnValueSelectListener() {
                @Override
                public void onValueSelected(int columnIndex, int subcolumnIndex, SubcolumnValue value) {
                    Log.d(TAG, "onValueDeselected: selected " + columnIndex);

                    try {
                        labelVolOne.setText("Date: ");
                        labelVolTwo.setText("Value: ");
                        labelVolThree.setText("Growth: ");

                        String text;

                        calendar.setTimeInMillis(Long.parseLong(points.get(columnIndex).date));
                        text = ""
                                + (calendar.get(Calendar.MONTH) + 1)
                                + "/" + calendar.get(Calendar.DAY_OF_MONTH)
                                + "/" + calendar.get(Calendar.YEAR);

                        volumeHighest.setText(text);

                        text = ""+points.get(columnIndex).getVolume();
                        volumeRcent.setText(text);

                        if (columnIndex == 0) {
                            text = "0%";
                        }else{
                            float percent = (points.get(columnIndex).getVolume()/points.get(columnIndex -1).getVolume()) -1;
                            text = ""+Math.round(100.0f * percent) + "%";
                        }
                        volumeGrowth.setText(text);
                    } catch (NumberFormatException e) {
                        Log.d(TAG, "onValueSelected: ",e);
                        setNormalLabels();
                        fillTextView();
                    }

                }

                @Override
                public void onValueDeselected() {
                    Log.d(TAG, "onValueDeselected: Deselected");
                    setNormalLabels();
                    fillTextView();
                }
            });


            weightView = (ColumnChartView) rootView.findViewById(R.id.weightChart);
            weightView.setInteractive(true);
            weightView.setZoomEnabled(false);
            weightView.setContainerScrollEnabled(false, ContainerScrollType.VERTICAL);
            weightView.setContainerScrollEnabled(false, ContainerScrollType.HORIZONTAL);
            weightView.setValueSelectionEnabled(true);
            weightView.setOnValueTouchListener(new ColumnChartOnValueSelectListener() {
                @Override
                public void onValueSelected(int columnIndex, int subcolumnIndex, SubcolumnValue value) {
                    try {
                        labelWeiOne.setText("Date: ");
                        labelWeiTwo.setText("Value: ");
                        labelWeiThree.setText("Growth: ");

                        String text;

                        calendar.setTimeInMillis(Long.parseLong(points.get(columnIndex).date));
                        text = ""
                                + (calendar.get(Calendar.MONTH) + 1)
                                + "/" + calendar.get(Calendar.DAY_OF_MONTH)
                                + "/" + calendar.get(Calendar.YEAR);

                        weightHighest.setText(text);
                        text = ""+points.get(columnIndex).getAverageWeight();
                        weightRecent.setText(text);

                        if (columnIndex == 0) {
                            text = "0%";
                        }else{
                            float percent = (points.get(columnIndex).getAverageWeight()/points.get(columnIndex -1).getAverageWeight()) -1;
                            text = ""+Math.round(100.0f * percent) + "%";
                        }
                        weightGrowth.setText(text);
                    } catch (NumberFormatException e) {
                        setNormalLabels();
                        fillTextView();
                    }
                }

                @Override
                public void onValueDeselected() {
                    setNormalLabels();
                    fillTextView();
                }
            });

            repView = (ColumnChartView) rootView.findViewById(R.id.repChart);
            repView.setInteractive(true);
            repView.setZoomEnabled(false);
            repView.setContainerScrollEnabled(false, ContainerScrollType.VERTICAL);
            repView.setContainerScrollEnabled(false, ContainerScrollType.HORIZONTAL);
            repView.setValueSelectionEnabled(true);

            recentVolume = (TextView) rootView.findViewById(R.id.tv_recent_volume);
            recentWeight = (TextView) rootView.findViewById(R.id.tv_recent_weight);
            recentRep = (TextView) rootView.findViewById(R.id.tv_recent_reps);

            volumeHighest = (TextView) rootView.findViewById(R.id.tv_volume_highest);
            volumeRcent = (TextView) rootView.findViewById(R.id.tv_volume_recent);
            volumeGrowth = (TextView) rootView.findViewById(R.id.tv_volume_growth);

            labelVolOne = (TextView) rootView.findViewById(R.id.volumeOne);
            labelVolTwo = (TextView) rootView.findViewById(R.id.volumeTwo);
            labelVolThree = (TextView) rootView.findViewById(R.id.volumeThree);

            weightHighest = (TextView) rootView.findViewById(R.id.tv_weight_highest);
            weightGrowth = (TextView) rootView.findViewById(R.id.tv_weight_growth);
            weightRecent = (TextView) rootView.findViewById(R.id.tv_weight_recent);

            labelWeiOne   = (TextView) rootView.findViewById(R.id.weightOne);
            labelWeiTwo   = (TextView) rootView.findViewById(R.id.weightTwo);
            labelWeiThree = (TextView) rootView.findViewById(R.id.weightThree);

            repRecent = (TextView) rootView.findViewById(R.id.tv_rep_recent);

            totalSession = (TextView) rootView.findViewById(R.id.tv_total_session);
            totalWeight = (TextView) rootView.findViewById(R.id.tv_total_weights);
            totalReps = (TextView) rootView.findViewById(R.id.tv_total_reps);
            totalSets = (TextView) rootView.findViewById(R.id.tv_total_sets);

        }
        return rootView;
    }

    public void setNormalLabels(){
        Resources res = getResources();
        labelVolOne.setText(res.getString(R.string.label_volume_highest));
        labelVolTwo.setText(res.getString(R.string.label_volume_recent));
        labelVolThree.setText(res.getString(R.string.label_volume_growth));
        labelWeiOne.setText(res.getString(R.string.label_weight_highest));
        labelWeiTwo.setText(res.getString(R.string.label_weight_recent));
        labelWeiThree.setText(res.getString(R.string.label_weight_growth));
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_edit, menu);
    }


    @Override
    public void onClick(View v) {

    }

    private void generateColumnData(List<FetchedDataPoint> points){

        //Volume Information
        List<Column> volumeColumns = new ArrayList<>();
        List<SubcolumnValue> volumeValues;

        for (int i = 0; i < points.size(); ++i) {
            volumeValues = new ArrayList<>();
            float volume = points.get(i).getVolume();
            SubcolumnValue  val = new SubcolumnValue(volume, ChartUtils.COLOR_ORANGE);
            val.setLabel(""+ volume );
            volumeValues.add(val);

            Column column = new Column(volumeValues);
            column.setHasLabels(true);
            column.setHasLabelsOnlyForSelected(true);
            volumeColumns.add(column);
        }
        volumeData = new ColumnChartData(volumeColumns);
        Axis volumeAxisY = new Axis().setHasLines(true);
        volumeAxisY.setName("Volume");
        volumeData.setAxisYLeft(volumeAxisY);
        volumnView.setColumnChartData(volumeData);

        //weight information
        List<Column> weightColumns = new ArrayList<>();
        List<SubcolumnValue> weightValues;
        for (int i = 0; i < points.size(); ++i) {

            weightValues = new ArrayList<>();
            float weight = points.get(i).getAverageWeight();
            SubcolumnValue  val = new SubcolumnValue(weight, ChartUtils.COLOR_BLUE);
            val.setLabel(""+weight);
            weightValues.add(val);

            Column column = new Column(weightValues);
            column.setHasLabels(true);
            column.setHasLabelsOnlyForSelected(true);
            weightColumns.add(column);
        }

        weightData = new ColumnChartData(weightColumns);
        Axis weightAxisY = new Axis().setHasLines(true);
        weightAxisY.setName("Weight");
        weightData.setAxisYLeft(weightAxisY);
        weightView.setColumnChartData(weightData);

        //General performance
        List<Column> repColumns = new ArrayList<>();
        List<SubcolumnValue> repValues;


        int i = 0;
        if(points.size() - 1 - 4 >0){
            i = points.size() - 1 - 4;
        }
        for ( i = points.size() - 1 - 4; i < points.size() && i >= 0; ++i) {

            int pointLenght = points.get(i).reps.length;
            repValues = new ArrayList<>();
            for (int j = 0; j < pointLenght; ++j) {

                float performance = points.get(i).getPerformance(j);
                SubcolumnValue  val = new SubcolumnValue(performance, ChartUtils.COLOR_GREEN);
                val.setLabel(points.get(i).getWeightbyIndex(j) + "x" + points.get(i).getRepbyIndex(j));
                repValues.add(val);
            }

            Column column = new Column(repValues);
            column.setHasLabels(true);
            column.setHasLabelsOnlyForSelected(true);
            repColumns.add(column);
        }

        repData = new ColumnChartData(repColumns);
        repData.setAxisYLeft(null);
        repData.setAxisXBottom(null);
        repView.setColumnChartData(repData);
    }

    public void fillTextView(){

        if (points.size() > 0) {

            String text = "" + points.get(points.size() - 1).getVolume();
            recentVolume.setText(text);
            volumeRcent.setText(text);

            text = "" + points.get(points.size() - 1).getAverageWeight();
            recentWeight.setText(text);
            weightRecent.setText(text);

            text = "" + points.get(points.size() - 1).getAverageRep();
            recentRep.setText(text);
            repRecent.setText(text);

            float highestVolume = 0.0f;
            float highestWeight = 0;

            int totalRepsValue = 0;
            float totalWeightsValue = 0.0f;
            int totalsetsValue = 0;

            for (int i = 0; i < points.size(); i++) {
                float temp = points.get(i).getVolume();
                float tempW = points.get(i).getHighestWeight();
                highestVolume = temp > highestVolume ? temp:highestVolume;
                highestWeight = tempW > highestWeight ? tempW : highestWeight;

                totalRepsValue += points.get(i).getTotalReps();
                totalWeightsValue += points.get(i).getTotalWeight();
                totalsetsValue += points.get(i).getTotalSets();
            }

            text = "" + highestVolume;
            volumeHighest.setText(text);
            text = "" + highestWeight;
            weightHighest.setText(text);

            //calculate growth
            float averageVolGrowth = 0.0f;
            float averageWeightGrowth = 0.0f;
            for (int i = 1; i < points.size(); i++) {
                averageVolGrowth +=  (points.get(i).getVolume()/points.get(i-1).getVolume() - 1) ;
                averageWeightGrowth +=  (points.get(i).getAverageWeight()/points.get(i-1).getAverageWeight() - 1);
            }
            averageVolGrowth = averageVolGrowth / (points.size() - 1);
            averageVolGrowth = Math.round(100f * averageVolGrowth);
            text  = ""+ averageVolGrowth+"%";
            volumeGrowth.setText(text);

            averageWeightGrowth = averageWeightGrowth / (points.size() - 1);
            averageWeightGrowth = Math.round(100f * averageWeightGrowth);
            text = "" + averageWeightGrowth +"%";
            weightGrowth.setText(text);

            text = "" + points.size();
            totalSession.setText(text);

            text = "" + totalRepsValue;
            totalReps.setText(text);

            text = "" + totalWeightsValue;
            totalWeight.setText(text);

            text = "" + totalsetsValue;
            totalSets.setText(text);
        }
    }

    public void parseData(String incoming){

        try {
            JSONArray json = new JSONArray(incoming);
            if(d_parseData) Log.e(TAG, "parseData() Length" + json.length());

            points = new ArrayList<>();

            if (collection == null) {
                loadCollection();
            }

            Gson gson = app.getGson();
            for (int i = 0; i < json.length(); i++) {

                JSONObject obj = (JSONObject)json.get(i);
                FetchedDataPoint point = gson.fromJson(obj.toString(), FetchedDataPoint.class);
                points.add(point);
            }
            generateColumnData(points);
            fillTextView();
        } catch (Exception e) {
            Log.e(TAG, "Error Parsing Json Input", e);
        }
    }
}
