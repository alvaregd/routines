package com.gorillamoa.routines.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.andexert.library.RippleView;
import com.gorillamoa.routines.R;
import com.gorillamoa.routines.activities.GroupListActivity;
import com.gorillamoa.routines.activities.ImportActivity;
import com.gorillamoa.routines.activities.SettingsActivity;
import com.gorillamoa.routines.interfaces.OnListFragmentInteractionListener;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class MainMenuFragment extends Fragment {

    private static final String ARG_COLUMN_COUNT = "column-count";

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MainMenuFragment() {}

    @SuppressWarnings("unused")
    public static MainMenuFragment newInstance(int columnCount) {
        MainMenuFragment fragment = new MainMenuFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_day,menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
/*
        switch (item.getItemId()) {
            case R.id.action_widget_editor:
                Intent intent = new Intent(getActivity(), WidgetEditorActivity.class);
                startActivity(intent);
            break;
        }
*/
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_menu, container, false);

        RippleView exercises = (RippleView) view.findViewById(R.id.main_exercises);
        RippleView imports = (RippleView) view.findViewById(R.id.main_import);
        RippleView settings = (RippleView) view.findViewById(R.id.main_settings);

        exercises.setRippleDuration(150);
        exercises.setRippleAlpha(150);
        exercises.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                Intent intent = new Intent(getActivity(), GroupListActivity.class);
                getActivity().startActivity(intent);
            }
        });

        imports.setRippleDuration(100);
        imports.setRippleAlpha(150);
        imports.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                Intent intent = new Intent(getActivity(), ImportActivity.class);
                getActivity().startActivity(intent);

            }
        });

        settings.setRippleDuration(150);
        settings.setRippleAlpha(150);
        settings.setOnRippleCompleteListener(new RippleView.OnRippleCompleteListener() {
            @Override
            public void onComplete(RippleView rippleView) {
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                getActivity().startActivity(intent);
            }
        });

        return view;
    }
}
