package com.gorillamoa.routines.fragments.dummy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample name for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class DummyContent {
    /**
     * An array of sample (dummy) items.
     */
    public static final List<DayEntry> ITEMS = new ArrayList<DayEntry>();

    /**
     * A map of sample (dummy) items, by ID.
     */
    public static final Map<String, DayEntry> ITEM_MAP = new HashMap<String, DayEntry>();

    private static final int COUNT = 7;

    static {
        // Add some sample items.
        for (int i = 1; i <= COUNT; i++) {
            addItem(createDays(i));
        }
    }

    private static void addItem(DayEntry item) {
        ITEMS.add(item);
        ITEM_MAP.put(item.id, item);
    }

    private static DayEntry createDays(int position) {
        return new DayEntry(String.valueOf(position),  position, makeDetails(position));
    }

    private static String makeDetails(int position) {
        StringBuilder builder = new StringBuilder();
        builder.append("Details about Item: ").append(position);
        for (int i = 0; i < position; i++) {
            builder.append("\nMore details information here.");
        }
        return builder.toString();
    }

    /**
     * A dummy item representing a piece of name.
     */
    public static class DayEntry {
        public final String id;
        public final String content;
        public final String details;

        public DayEntry(String id, int content, String details) {
            this.id = id;

            switch (content) {
                case 1:  this.content ="Sunday"; break;
                case 2:  this.content ="Monday";break;
                case 3:  this.content ="Tuesday";break;
                case 4:  this.content ="Wednesday";break;
                case 5:  this.content ="Thursday";break;
                case 6:  this.content ="Friday";break;
                case 7:  this.content ="Saturday";break;
                default:
                    this.content = "Something bad happened";
            }
            this.details = details;
        }

        @Override
        public String toString() {
            return content;
        }
    }
}
