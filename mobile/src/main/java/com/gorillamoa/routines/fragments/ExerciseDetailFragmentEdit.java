package com.gorillamoa.routines.fragments;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Switch;

import com.gorillamoa.routines.R;
import com.gorillamoa.routines.activities.ExerciseDetailActivity;
import com.gorillamoa.routines.activities.ExerciseListActivity;
import com.gorillamoa.routines.data.Exercise;
import com.gorillamoa.routines.app.App;
import com.gorillamoa.routines.data.ExerciseCollection;
import com.gorillamoa.routines.data.ExerciseGroup;

import lecho.lib.hellocharts.view.ColumnChartView;

/**
 * A fragment representing a single Exercise detail screen.
 * This fragment is either contained in a {@link ExerciseListActivity}
 * in two-pane mode (on tablets) or a {@link ExerciseDetailActivity}
 * on handsets.
 */
public class ExerciseDetailFragmentEdit extends Fragment{

    private static final String TAG = ExerciseDetailFragmentEdit.class.getName();
    public static final boolean d_onCreate= true;
    public static final boolean d_onOptionsItemSelected = true;

    private boolean isNew;

    private ExerciseCollection collection;
    private ExerciseGroup group;
    private Exercise exercise;

    CollapsingToolbarLayout appBarLayout;

    private EditText etName, etSets, etRepsMin, etRepsMax,etRepIncrement, etWeight, etWeightIncrement,etNotes;
    private Switch incrementReps, incrementWeight;
    private View nameLayout;

    private int selectedGroup;
    private int selectedExercise;

    App app;

    public interface EditListener{
         void onSaveClicked();
    }

    private EditListener listener;

    /*** The fragment argument representing the item ID that this fragment represents.*/
    public static final String ARG_EXERCISE_ID = "item_id";
    public static final String ARG_GROUP_ID = "group_id";
    public static final String ARG_ISEDITING = "isNew";

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            listener = (EditListener) context;
        } catch (ClassCastException e) {
            Log.d(TAG, "onAttach: Problem Attach listener",e);
        }
    }

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ExerciseDetailFragmentEdit() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadCollection();

        Activity activity= getActivity();

        //selected group is always selected
        selectedGroup = Integer.parseInt(getArguments().getString(ARG_GROUP_ID));

        //Check for existing arguments
        if (getArguments().containsKey(ARG_GROUP_ID) && getArguments().containsKey(ARG_EXERCISE_ID)) {

            //check if we are editing
            if (getArguments().getString(ARG_GROUP_ID) != null && getArguments().getString(ARG_EXERCISE_ID) != null) {

                selectedExercise = Integer.parseInt(getArguments().getString(ARG_EXERCISE_ID));
                Log.d(TAG, "group id: " + selectedGroup);
                Log.d(TAG, "exercise id: " + selectedExercise);
                group = collection.getExerciseGroup(selectedGroup);
                exercise = group.getExercises().get(selectedExercise);
            }

            setHasOptionsMenu(true);

            if (exercise != null) {
                //This is an old entry we are editing
                isNew = false;

                if(d_onCreate) Log.e(TAG, "onCreate() " + "Exercise OK " + selectedExercise);

                //Modify the title
                appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
                if (appBarLayout != null) {

                    appBarLayout.setTitle(exercise.name);
                }
            }else {
                //this is a new entry
                isNew = true;

                //Modify the title
                appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
                if (appBarLayout != null) {
                    appBarLayout.setTitle("New Exercise");
                }
            }
        }else{
            if(!getArguments().containsKey(ARG_GROUP_ID)){
                Log.d(TAG, "NO group id ");
            }else if(getArguments().containsKey(ARG_EXERCISE_ID)){
                Log.d(TAG, "No exercise Id");
            }
        }
    }

    private void loadCollection() {
        app = ((App)getActivity().getApplicationContext());
        collection = App.LoadCollection(app.getPreferences(),app.getGson());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView;

        rootView = inflater.inflate(R.layout.exercise_detail_edit, container, false);

        etName = (EditText) rootView.findViewById(R.id.name);
        etName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                appBarLayout.setTitle(s);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        etNotes = (EditText) rootView.findViewById(R.id.note);
        etRepsMin = (EditText) rootView.findViewById(R.id.rep_min);
        etRepsMax = (EditText) rootView.findViewById(R.id.rep_max);
        etSets = (EditText) rootView.findViewById(R.id.sets);
        etWeight = (EditText) rootView.findViewById(R.id.weight_number);
//        incrementReps = (Switch) rootView.findViewById(R.id.incReps);
//        incrementWeight = (Switch) rootView.findViewById(R.id.incWeight);
//        etWeightIncrement = (EditText) rootView.findViewById(R.id.etWeightIncrement);
//        etRepIncrement = (EditText) rootView.findViewById(R.id.etRepIncrement);
        etNotes = (EditText) rootView.findViewById(R.id.note);
        nameLayout = rootView.findViewById(R.id.layout_name);

        fillData();
        return rootView;

    }

    private void fillData() {

        if (!isNew) {
            Log.d(TAG, "fillData: isEditing");
            etName.setText(String.valueOf(exercise.name));
            etSets.setText(String.valueOf(exercise.sets));
            etWeight.setText(String.valueOf(exercise.weight));
            etRepsMin.setText(String.valueOf(exercise.repsMin));
            etRepsMax.setText(String.valueOf(exercise.repsMax));
//            incrementWeight.setChecked(exercise.isWeightIncrement);
//            incrementReps.setChecked(exercise.isRepIncrement);
//            etWeightIncrement.setText(String.valueOf(exercise.weightIncrement));
//            etRepIncrement.setText(String.valueOf(exercise.repIncrement));
            etNotes.setText(exercise.note);
            appBarLayout.setTitle(etName.getText().toString());
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (collection == null) {
            loadCollection();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_edit, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save:
                if (validated()) {
                    if (collection != null) {
                        if (isNew) {
                            collection.getGroups().get(selectedGroup).getExercises().add(
                                    getNewExercise()
                            );
                            //TODO
                            if(d_onOptionsItemSelected) Log.e(TAG, "onOptionsItemSelected() New Entry ");
                        }else {
                            //not a new entry
                            updateExercise();
                            if(d_onOptionsItemSelected) Log.e(TAG, "onOptionsItemSelected() Save Entry");
                        }
                    }
                    App app = (App)getActivity().getApplicationContext();
                    App.saveCollection(collection,app.getPreferences(), app.getGson());
                    listener.onSaveClicked();
                }else{
                    if(d_onOptionsItemSelected) Log.e(TAG, "onOptionsItemSelected() Not validated");
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateExercise() {

        exercise.name =  etName.getText().toString();
        exercise.type = Exercise.ExerciseType.Standard;
        exercise.sets = Integer.valueOf(etSets.getText().toString());
        exercise.repsMin = Integer.valueOf(etRepsMin.getText().toString());
        exercise.repsMax =Integer.valueOf(etRepsMax.getText().toString());
//        exercise.repIncrement = Integer.valueOf(etRepIncrement.getText().toString());
        exercise.weight = Float.valueOf(etWeight.getText().toString());
//        exercise.weightIncrement= Float.valueOf(etWeightIncrement.getText().toString());
        exercise.dayOfWeek = 1;
//        exercise.isRepIncrement= incrementReps.isChecked();
//        exercise.isWeightIncrement = incrementWeight.isChecked();
        exercise.note = etNotes.getText().toString();
    }

    public boolean validated() {
        boolean isValidationSuccessful = true;

        if (isValidationSuccessful && (Integer.valueOf(etRepsMax.getText().toString()) < Integer.valueOf(etRepsMin.getText().toString()))) {
            etRepsMax.setError("Min Rep must be smaller than Max Reps");
            etRepsMin.setError("Min Rep must be smaller than Max Reps");
            isValidationSuccessful = false;
        }

        //check fields are numbers only etc..
        return isValidationSuccessful;
    }


    public Exercise getNewExercise() {
        return new Exercise(
                //TODO
//                collection.getnewId(day),
                ""+collection.getGroups().get(selectedGroup).getExercises().size(),
                etName.getText().toString(),
                Exercise.ExerciseType.Standard,
                Integer.valueOf(etSets.getText().toString()),
                Integer.valueOf(etRepsMin.getText().toString()),
                Integer.valueOf(etRepsMax.getText().toString()),
                Integer.valueOf(etRepIncrement.getText().toString()),
                Float.valueOf(etWeight.getText().toString()),
                Float.valueOf(etWeightIncrement.getText().toString()),
                1,
                incrementReps.isChecked(),
                incrementWeight.isChecked(),
                etNotes.getText().toString()
        );
    }

    public boolean getNewStatus(){
        return isNew;
    }

}

