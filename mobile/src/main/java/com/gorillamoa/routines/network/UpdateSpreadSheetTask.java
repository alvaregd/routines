package com.gorillamoa.routines.network;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.auth.GoogleAuthException;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.script.model.ExecutionRequest;
import com.google.api.services.script.model.Operation;
import com.gorillamoa.routines.app.App;
import com.gorillamoa.routines.data.DataPointCollection;
import com.gorillamoa.routines.data.Datapoint;
import com.gorillamoa.routines.interfaces.NetworkCallbacks;
import com.gorillamoa.routines.utils.CryptoUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by alvaregd on 15/11/15.
 * Handles the Saving of exercises
 */
public class UpdateSpreadSheetTask extends AsyncTask<Datapoint, Void, List<String>> {

    private static final String TAG = UpdateSpreadSheetTask.class.getName();
    private com.google.api.services.script.Script mService = null;
    private Exception mLastError = null;

    private Context context;
    private NetworkCallbacks callback;

    private Datapoint params;

    public void setParams(Datapoint params) {
        this.params = params;
    }


    public UpdateSpreadSheetTask(Context context,NetworkCallbacks callbacks){

        this.context = context;
        this.callback = callbacks;
        init(context);
    }

    public UpdateSpreadSheetTask(Context context) {

        this.context= context;
        init(context);
    }

    public void sendDataToRemote() {
        this.execute(params);
    }

    private  void init(Context context){
        //Fetch the name
        SharedPreferences settings = context.getSharedPreferences(App.FORM_PREFS, Context.MODE_PRIVATE);

        //if name is num;;
        GoogleAccountCredential mCredential = GoogleAccountCredential.usingOAuth2(
                context, Arrays.asList(NetworkUtils.SCOPES))
                .setBackOff(new ExponentialBackOff())
                .setSelectedAccountName(settings.getString(App.USER_KEY, null));

        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        mService = new com.google.api.services.script.Script.Builder(
                transport, jsonFactory, NetworkUtils.setHttpTimeout(mCredential))
                .setApplicationName("Routines Connect")
                .build();
    }

    /**
     * Background task to call Google Apps Script Execution API.
     *
     * @param params no parameters needed for this task.
     */
    @Override
    protected List<String> doInBackground(Datapoint... params) {
        try {
            return getDataFromApi(params[0]);
        } catch (Exception e) {
            mLastError = e;
            cancel(true);
            return null;
        }
    }

    /**
     * Call the API to run an Apps Script function that returns a list
     * of folders within the user's root directory on Drive.
     *
     * @return list of String folder names and their IDs
     * @throws IOException
     */
    private List<String> getDataFromApi(Datapoint params)
            throws IOException, GoogleAuthException {
        // ID of the script to call. Acquire this from the Apps Script editor,
        // under Publish > Deploy as API executable.
        String scriptId = "MR371DZMGOwaZA0otZtnqoBnNlfM_PQae";

        List<String> folderList = new ArrayList<>();
        List<Object> parameters = new ArrayList<>();

        parameters.add(params.date);
        parameters.add(params.routine);
        parameters.add(params.name);
        parameters.add(params.weight);
        parameters.add(params.reps);
        // Create an execution request object.
        ExecutionRequest request = new ExecutionRequest()
                .setFunction("insertDataPoint")
                .setParameters(parameters);

        // Make the request.
        Operation op =
                mService.scripts().run(scriptId, request).execute();

        // Print results of request.
        if (op.getError() != null) {
            throw new IOException(getScriptError(op));
        }
        if (op.getResponse() != null &&
                op.getResponse().get("result") != null) {

            Log.d(TAG, "getDataFromApi: " + op.getResponse().get("result").toString());
        }

        return folderList;
    }

    /**
     * Interpret an error response returned by the API and return a String
     * summary.
     *
     * @param op the Operation returning an error response
     * @return summary of error response, or null if Operation returned no
     * error
     */
    private String getScriptError(Operation op) {
        if (op.getError() == null) {
            return null;
        }
        // Extract the first (and only) set of error details and cast as a Map.
        // The values of this map are the script's 'errorMessage' and
        // 'errorType', and an array of stack trace elements (which also need to
        // be cast as Maps).
        Map<String, Object> detail = op.getError().getDetails().get(0);
            List<Map<String, Object>> stacktrace =
                    (List<Map<String, Object>>) detail.get("scriptStackTraceElements");

        java.lang.StringBuilder sb =
                new StringBuilder("\nScript error message: ");
        sb.append(detail.get("errorMessage"));

        if (stacktrace != null) {
            // There may not be a staEcktrace if the script didn't start
            // executing.
            sb.append("\nScript error stacktrace:");
            for (Map<String, Object> elem : stacktrace) {
                sb.append("\n  ");
                sb.append(elem.get("function"));
                sb.append(":");
                sb.append(elem.get("lineNumber"));
            }
        }
        sb.append("\n");
        return sb.toString();
    }

    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onPostExecute(List<String> output) {
        if (output == null || output.size() == 0) {
            Log.d(TAG, "onPostExecute: No results returned");
        } else {
            Log.d(TAG, "onPostExecute: " + output);
        }

        String key = CryptoUtils.MD5(params.routine, params.name);
        App app = (App)context.getApplicationContext();
        SharedPreferences cacheStatusPref = app.getCacheStatusPreferences();
        SharedPreferences.Editor editor = cacheStatusPref.edit();
        editor.putBoolean(key, false);
        editor.apply();

        if(callback != null){
            Log.d(TAG, "onPostExecute: Launched from Activity, closing it");
            callback.OnSuccess();
        }
    }

    @Override
    protected void onCancelled() {
        if (mLastError != null) {
            if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                Log.d(TAG, "onCancelled: GooglePlayServicesAvailabilityIOException");
            } else if (mLastError instanceof UserRecoverableAuthIOException) {
                Log.d(TAG, "onCancelled: UserRecoverableAuthIOException");
                if (callback != null) {
                    Log.d(TAG, "onCancelled: Callbackings");
                    callback.AuthException(mLastError);
                }
            } else {
                Log.d(TAG, "onCancelled: The following error occurred:" + mLastError.getMessage());
                Toast.makeText(this.context,"Remote save failed, saving locally",Toast.LENGTH_SHORT).show();
            }
        } else {
            Log.d(TAG, "onCancelled: Request cancelled");
            Toast.makeText(this.context, "Remote save failed, saving locally", Toast.LENGTH_SHORT).show();
            App app = ((App) context.getApplicationContext());
            DataPointCollection dataPointCollection = app.getDataPointCollection();
            dataPointCollection.getPoints().add(params);
            App.saveDataPointCollection(dataPointCollection, app.getPreferences(), app.getGson());
        }
    }
}