package com.gorillamoa.routines.services;

import android.content.Intent;
import android.widget.RemoteViewsService;

import com.gorillamoa.routines.providers.ListProvider;

/**
 * Created by alvaregd on 26/01/16.
 */

public class ListService  extends RemoteViewsService{

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
/*
        int appWidgetId = intent.getIntExtra(
                AppWidgetManager.EXTRA_APPWIDGET_ID,
                AppWidgetManager.INVALID_APPWIDGET_ID
        );
*/
        return (new ListProvider(this.getApplicationContext(), intent));
    }
}
