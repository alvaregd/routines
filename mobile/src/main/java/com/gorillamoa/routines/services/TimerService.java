package com.gorillamoa.routines.services;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import com.gorillamoa.routines.providers.ExerciseProvider;

/**
 * Created by alvaregd on 15/12/15.
 */
public class TimerService extends Service {

    private static final String TAG = TimerService.class.getName();
    public static final String TIMER_UPDATE = "TIMER_UPDATE";

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent == null) {
            Log.d(TAG, "onStartCommand() Intent null");
            return super.onStartCommand(null, flags, startId);
        }

        String action = intent.getAction();
        if (action == null) {
            Log.d(TAG, "onStartCommand() Action null");
            return super.onStartCommand(intent, flags, startId);
        }

        Log.d(TAG, String.format("onStartCommand: %s", action));

        if (action.equals(TIMER_UPDATE)) {

            AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplicationContext());
            int[] allWidgetIds = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);

//            RemoteViews views = new RemoteViews(this.getPackageName(), R.layout.clock);
            RemoteViews views = ExerciseProvider.setWidgetUIContent(null,getApplicationContext(),"Start!", allWidgetIds[0]);
            for (int widgetId : allWidgetIds) {
                appWidgetManager.updateAppWidget(widgetId, views);
            }
            Log.d(TAG, "Should stop Self");
            stopSelf();
        }
        return super.onStartCommand(intent, flags, startId);
    }
}
