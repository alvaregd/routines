package com.gorillamoa.routines.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.multidex.MultiDex;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.gorillamoa.routines.data.DataPointCollection;
import com.gorillamoa.routines.data.Exercise;
import com.gorillamoa.routines.data.ExerciseCollection;
import com.gorillamoa.routines.data.ExerciseGroup;
import com.gorillamoa.routines.data.ExerciseProgress;
import com.gorillamoa.routines.data.AppearanceSettings;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.List;

/**
 * Created by alvaregd on 12/11/15.
 * manages the collection entity
 */
public class App extends Application {
    private static final String TAG = App.class.getName();
    private static final boolean d_getWidgetAppearanceSettings = true;

    public final static String FORM_PREFS = "LOCALSTORAGE";                  //app preferences
    public final static String COLLECTION_KEY = "COLLKEY";                   //exercise collection key
    public final static String USER_KEY = "USRKEY";                          //user specific settings
    public final static String APPEARANCE_KEY = "APRNCKEY";                  //widget appearance key
    public static final String LOCAL_DATA_STORAGE_KEY = "LOCAL_STORAGE_KEY"; //store data that still needs to be saved to server
                                                                             //but can't because no connectivity or other reason

    public static final String CACHE_PREF_KEY = "CACHESTATUSKEY";             //place to store cache status
    public static final String CACHE_STATUS_PREF_KEY = "CACHEKEY";                    //cached data from the network

    AppearanceSettings appearanceSettings;
    ExerciseCollection collection;
    Gson gson;
    SharedPreferences collectionPref;  //place to store exercise collection
    SharedPreferences cacheStatusPref; //place to store cache status
    SharedPreferences cachePref;       //place to store cached data
    DataPointCollection pointCollection;

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(base);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        collectionPref = getSharedPreferences(FORM_PREFS, Activity.MODE_PRIVATE);
        gson = new Gson();
        collection =  LoadCollection(collectionPref,gson);
    }

    public static boolean isNameNotNull(Context context){

        SharedPreferences settings = context.getSharedPreferences(FORM_PREFS, Context.MODE_PRIVATE);
        String name = settings.getString(USER_KEY, null);
        return name != null;
    }

    public static String fetchName(Context context){
        SharedPreferences settings = context.getSharedPreferences(FORM_PREFS, Context.MODE_PRIVATE);
        return settings.getString(USER_KEY, null);
    }

    public AppearanceSettings getWidgetAppearanceSettings() {
        if(d_getWidgetAppearanceSettings) Log.e(TAG, "getWidgetAppearanceSettings() ");
            String data = getPreferences().getString(APPEARANCE_KEY,"");

        if(d_getWidgetAppearanceSettings) Log.e(TAG, "getWidgetAppearanceSettings() "+data);
            if (data.isEmpty() || data.equals("")) {
                appearanceSettings = new AppearanceSettings();
            }else {
                if(d_getWidgetAppearanceSettings) Log.e(TAG, "getWidgetAppearanceSettings() Gson Loaded");
                appearanceSettings = gson.fromJson(data, AppearanceSettings.class);
            }
        return appearanceSettings;
    }

    public boolean importRoutine(String data) {

        ExerciseCollection collection = new ExerciseCollection();
        Type listType = new TypeToken<List<Exercise>>() {}.getType();

        try {
            Log.d(TAG, "importRoutine: Converting from string");

            JSONObject json = new JSONObject(data);

            Iterator<?> keys = json.keys();
            while (keys.hasNext()) {


                String key = (String) keys.next();
                Log.d(TAG, "importRoutine: Key:" + key);

                if (json.get(key) instanceof JSONArray) {

                    ExerciseGroup group = new ExerciseGroup();
                    //get the name
                    group.setGroupName(key);
                    //get the exercise array
                    List<Exercise> exercises = gson.fromJson(json.get(key).toString(), listType);
                    group.setExercises(exercises);
                    //add the group tothe colleciton
                    collection.getGroups().add(group);

                }
            }

        } catch (Exception e) {

            Log.e(TAG, "importRoutine: ", e);
            return false;
        }

        Log.d(TAG, "importRoutine: Saving...");
        collection.setProgress(new ExerciseProgress());
        saveCollection(
                collection,
                getPreferences(),
                getGson()
        );
        Log.d(TAG, "importRoutine: Updating");
        this.collection = collection;
        Toast.makeText(getApplicationContext(), "Import Successful", Toast.LENGTH_SHORT).show();
        return true;
    }


    public static ExerciseCollection LoadCollection(SharedPreferences prefs, Gson gson) {
        ExerciseCollection collection;
        String data = prefs.getString(COLLECTION_KEY, "");
        if (data.isEmpty() | data.equals("")) {
            collection = new ExerciseCollection();
            collection.init();
        }else {
            collection = gson.fromJson(data, ExerciseCollection.class);
            Log.d(TAG, "LoadCollection: Load Successful");
        }
        return collection;
    }


    public static void saveCollection(ExerciseCollection collection, SharedPreferences prefs, Gson gson) {

        if (collection != null) {
            String data = gson.toJson(collection);
            Log.d(TAG, "saveCollection: " + data);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putString(COLLECTION_KEY, data);
            edit.apply();
        }
    }
    public static void saveAppearanceSettings(AppearanceSettings settings, SharedPreferences prefs, Gson gson) {

        if (settings != null) {
            String data = gson.toJson(settings);
            Log.d(TAG, "saveAPperance: " + data);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putString(APPEARANCE_KEY, data);
            edit.apply();
        }
    }

    public void clearData(){
        if (collection != null) {
            String data = "";
            SharedPreferences.Editor edit = collectionPref.edit();
            edit.putString(COLLECTION_KEY, data);
            edit.apply();
            collection = new ExerciseCollection();
            collection.init();
        }
    }

    public Gson getGson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }

    public SharedPreferences getPreferences() {
        if (collectionPref == null) {
            collectionPref = getSharedPreferences(FORM_PREFS, Activity.MODE_PRIVATE);
        }
        return collectionPref;
    }

    private DataPointCollection LoadDataPointCollection(SharedPreferences prefs, Gson gson) {
        DataPointCollection pointCollection;
        String data = prefs.getString(LOCAL_DATA_STORAGE_KEY, "");
        if (data.isEmpty() | data.equals("")) {
            pointCollection = new DataPointCollection();
            pointCollection.init();
            Log.d(TAG, "loadDataPointCollection() : New instance");
        }else {
            pointCollection = gson.fromJson(data, DataPointCollection.class);
            Log.d(TAG, "loadDataPointCollection() : Load Successfully");
        }
        return pointCollection;
    }

    public DataPointCollection getDataPointCollection() {
        if (pointCollection == null) {
            pointCollection= LoadDataPointCollection(getPreferences(), getGson());
        }
        return pointCollection;
    }

    public static void saveDataPointCollection(DataPointCollection dataPointCollection, SharedPreferences prefs, Gson gson) {

        if (dataPointCollection != null) {
            String data = gson.toJson(dataPointCollection);
            SharedPreferences.Editor edit = prefs.edit();
            edit.putString(LOCAL_DATA_STORAGE_KEY, data);
            edit.apply();
        }
    }

    public  SharedPreferences getCacheStatusPreferences(){
        if (cacheStatusPref == null) {
            cacheStatusPref = getSharedPreferences(CACHE_STATUS_PREF_KEY, Activity.MODE_PRIVATE);
        }
        return cacheStatusPref;
    }
    public  SharedPreferences getCachePreferences(){
        if (cachePref == null) {
            cachePref = getSharedPreferences(CACHE_PREF_KEY, Activity.MODE_PRIVATE);
        }
        return cachePref;
    }
}
