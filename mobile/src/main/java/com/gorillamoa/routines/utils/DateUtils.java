package com.gorillamoa.routines.utils;


import com.gorillamoa.routines.app.App;

import java.util.Calendar;

/**
 * Created by alvaregd on 12/11/15.
 */
public class DateUtils {

    public static int getDayofWeek() {
        Calendar date = Calendar.getInstance();
        return date.get(Calendar.DAY_OF_WEEK);
    }

    public static String getStringDate() {

        StringBuilder builder = new StringBuilder();
        Calendar date = Calendar.getInstance();
        builder.append(date.get(Calendar.YEAR));
        builder.append("/");
        builder.append(date.get(Calendar.MONTH) + 1);
        builder.append("/");
        builder.append(date.get(Calendar.DAY_OF_MONTH));
        return builder.toString();
    }


}
