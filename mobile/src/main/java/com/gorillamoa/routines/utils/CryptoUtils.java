package com.gorillamoa.routines.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by alvaregd on 09/02/16.
 */
public class CryptoUtils {

    public static String MD5(String a, String b) {

        try {
            String concat = a + b;
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(concat.getBytes());
            byte[] digest = md.digest();
            StringBuffer sb = new StringBuffer();
            for (byte c : digest) {
                sb.append(String.format("%02x", c & 0xff));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            return "";
        }
    }
}
