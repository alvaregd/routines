package com.gorillamoa.routines.utils;

import com.gorillamoa.routines.data.ExerciseCollection;

/**
 * Created by alvaregd on 26/01/16.
 * Generates object for testing purposes
 */
public class TestFactory {

    public static ExerciseCollection getFakeCollection(){
        ExerciseCollection collection = new ExerciseCollection();
        collection.generateFakeData();
        return collection;
    }

}
