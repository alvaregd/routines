package com.gorillamoa.routines.managers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.gorillamoa.routines.R;
import com.gorillamoa.routines.activities.ResultFetchingActivity;
import com.gorillamoa.routines.app.App;
import com.gorillamoa.routines.data.Datapoint;
import com.gorillamoa.routines.data.Exercise;
import com.gorillamoa.routines.data.ExerciseCollection;
import com.gorillamoa.routines.data.ExerciseGroup;
import com.gorillamoa.routines.data.ExerciseProgress;
import com.gorillamoa.routines.network.UpdateSpreadSheetTask;
import com.gorillamoa.routines.utils.DateUtils;

/**
 * Helper class for providing sample name for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class ExerciseManager {

    private static final String TAG = ExerciseManager.class.getName();

    private static final boolean d_getNextValues = true;
    private static final boolean d_getExerciseValues = true;
    public static final boolean d_endExercise = true;
    public static final boolean d_startExercise = true;

    public static final String NO_ACTIVE_GROUP= "NO_ACTIVE_GROUP";
    public static final String ALL_EXERCISES_DONE = "DONE_EXERCIES";

    private static Datapoint params;

    public final static int SET = 0;
    public final static int REP = 1;
    public final static int WEIGHT = 2;
    public final static int NAME = 3;
    public final static int PROGRESS = 4;
    public final static int TIME_ELAPSED = 4;

    public static String[] getExerciseValues(ExerciseCollection collection, Context context) {
        if (d_getExerciseValues) Log.e(TAG, "getExerciseValues() Enter");

        try{
            ExerciseProgress progress = collection.getProgress();

            if (d_getExerciseValues)
                Log.e(TAG, "getExerciseValues() check ID " + progress.getCurrentGoup());

            //Do we have an active group?
            if (!progress.isActive()) {
                if (d_getExerciseValues) Log.e(TAG, "getExerciseValues() No Active Groups");
                String[] data = new String[5];
                data[0] = data[1] = data[2] = data[3] = data[4] = "-1";
                data[SET] = NO_ACTIVE_GROUP;
                return data;
            }

            if (!progress.isFinished()) {
                if (d_getExerciseValues) Log.e(TAG, "getExerciseValues() Not finished yet");
                //get exercise detail
                ExerciseGroup group = collection.getExerciseGroup(progress.getCurrentGoup());
                Exercise exercise = group.getExercises().get(progress.getCurrentExercise());

                String[] data = new String[5];
                data[SET] = String.valueOf(progress.getCurrentSet() + 1);
                data[REP] = String.valueOf(progress.getEditRepValue() != -1 ? progress.getEditRepValue() : exercise.getRep());
                data[NAME] = exercise.name;
                data[WEIGHT] = String.valueOf(progress.getEditWeightValue() != -1 ? progress.getEditWeightValue() : exercise.getWeight());
                data[PROGRESS] = String.valueOf(Math.round(100f * progress.getPercentageProgress())) + "%";
                return data;
            }else{

                if (d_getExerciseValues) Log.e(TAG, "getExerciseValues() finished");
                String[] data = new String[5];
                data[0] = data[1] = data[2] = data[3] = "-1";
                data[4] = progress.getWorkoutTime();
                data[SET] = ALL_EXERCISES_DONE;
                return data;
            }

        }catch (NullPointerException e ){

            if (d_getExerciseValues) Log.e(TAG, "getExerciseValues() Null Pointer");
            String[] data = new String[5];
            data[0] = data[1] = data[2] = data[3] = data[4] = "-1";
            data[SET] = NO_ACTIVE_GROUP;
            return data;
        }
    }

    public static void calculateNextExerciseValues(ExerciseCollection collection,Context context ) {
        if (d_getNextValues) Log.e(TAG, "calculateNextExerciseValues() + Enter");

        try {
            ExerciseProgress progress = collection.getProgress();
            ExerciseGroup group = collection.getExerciseGroup(progress.getCurrentGoup());
            Exercise exercise = group.getExercises().get(progress.getCurrentExercise());

            progress.advance();


            //Check that we actually got an exercise object
            if (exercise == null) {
                Log.d(TAG, "calculateNextExerciseValues: Exercise retrieval Unsuccessful");
                return;
            }

            // Record the exercise in the excel spreadsheet
            if (params == null) {
                params = new Datapoint();
            }

            params.date = DateUtils.getStringDate();
            params.routine = group.getGroupName();
            params.name = exercise.name;

            params.weight = progress.getEditWeightValue() != -1 ? progress.getEditWeightValue() : exercise.getWeight();
            params.reps = progress.getEditRepValue() != -1 ? progress.getEditRepValue() : exercise.getRep();

            progress.setEditRepValue(-1);
            progress.setEditWeightValue(-1);

            if (d_getNextValues) Log.e(TAG, "calculateNextExerciseValues()() + Date "    + params.date);
            if (d_getNextValues) Log.e(TAG, "calculateNextExerciseValues()() + NAME "    + params.name);
            if (d_getNextValues) Log.e(TAG, "calculateNextExerciseValues()() + REP "     + params.reps);
            if (d_getNextValues) Log.e(TAG, "calculateNextExerciseValues()() + WEIGHT "  + params.weight);
            if (d_getNextValues) Log.e(TAG, "calculateNextExerciseValues()() + ROUTINE " + params.routine);

            //Synchronize data locally
            if (App.isNameNotNull(context)) {
                UpdateSpreadSheetTask task = new UpdateSpreadSheetTask(context);
                task.setParams(params);
                task.sendDataToRemote();
            } else {
                //app is not connected to any accounts, bring up the account picker
                Intent intent = new Intent(context, ResultFetchingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                Bundle extra = new Bundle();
                extra.putString(Datapoint.PARAM_DATE, params.date);
                extra.putString(Datapoint.PARAM_NAME, params.name);
                extra.putString(Datapoint.PARAM_ROUTINE, params.routine);
                extra.putInt(Datapoint.PARAM_REPS, params.reps);
                extra.putFloat(Datapoint.PARAM_WEIGHT, params.weight);
                intent.putExtras(extra);
                context.startActivity(intent);
            }



            //Are we done the current exercise?
            if (exercise.isFinishedExercise(progress.getCurrentSet())) {

                if (d_getNextValues) Log.e(TAG, "calculateNextExerciseValues() new exercise");
                exercise.incrementAttemps();
                progress.nextExercise();
            }

            if (exercise.attribute0 == Exercise.ATTRIB_MAX_REST) {
                progress.setTimeRemaining(exercise.value0);
            }

        } catch (NullPointerException e) {
            Log.e(TAG, "calculateNextExerciseValues: Error",e );
        }
    }

    public static void endActivity(ExerciseCollection collection) {
        if (d_endExercise) Log.e(TAG, "endActivity() Enter");
        ExerciseProgress progress = collection.getProgress();
        progress.setCurrentExercise(0);
        progress.setCurrentSet(0);
        progress.setProgress(0);
        progress.setEditRepValue(-1);
        progress.setEditRepValue(-1);
        progress.setTimeRemaining(0);
    }

    public static void startActivity(ExerciseCollection collection, int id) {
        if (d_startExercise) Log.e(TAG, "startActivity() Enter");
        if (d_startExercise) Log.e(TAG, "startActivity() Start Exercise Id: " + id);

        //get the exercise information
        ExerciseGroup group = collection.getExerciseGroup(id);
        int size = group.getExercises().size();

        ExerciseProgress progress = collection.getProgress();
        progress.setRoutine(id, size, group.getTotalGroupSets());

    }

    public static String getNote(ExerciseCollection collection) {

        String note;
        try {
            ExerciseProgress progress = collection.getProgress();
            ExerciseGroup group = collection.getExerciseGroup(progress.getCurrentGoup());
            Exercise exercise = group.getExercises().get(progress.getCurrentExercise());
            note = exercise.note;
        } catch (IndexOutOfBoundsException e) {

            Log.e(TAG, "getNote: Error ", e);
            note = "No Available Note";
        }
        return note;
    }

    public static String getQuote(Context context) {

        switch (DateUtils.getDayofWeek()) {
            case 1:
                return context.getResources().getString(R.string.motivation_1);
            case 2:
                return context.getResources().getString(R.string.motivation_2);
            case 3:
                return context.getResources().getString(R.string.motivation_3);
            case 4:
                return context.getResources().getString(R.string.motivation_4);
            case 5:
                return context.getResources().getString(R.string.motivation_5);
            case 6:
                return context.getResources().getString(R.string.motivation_6);
            case 7:
                return context.getResources().getString(R.string.motivation_7);
            default:
                return "Nice Work!";
        }
    }

    public static void changeValuesOfExericse(Context context, ExerciseCollection collection) {

        ExerciseProgress progress = collection.getProgress();
        ExerciseGroup group = collection.getExerciseGroup(progress.getCurrentGoup());
        Exercise exercise = group.getExercises().get(progress.getCurrentExercise());

        if (progress.getEditWeightValue() != -1) {
            exercise.setStartingWeightValue(progress.getEditWeightValue());
        } else if (progress.getEditRepValue() != -1) {
            exercise.setStartingRepValue(progress.getEditRepValue());
        }
        progress.setEditWeightValue(-1);
        progress.setEditRepValue(-1);
    }

    public static void nextExercise(ExerciseCollection collection){

        ExerciseProgress progress = collection.getProgress();
        int exerciseTotal = progress.getFinishRecords().length;

        if (progress.getCurrentExercise() < exerciseTotal -1 && exerciseTotal > 1) {
            //increment
            progress.setCurrentSet(0);
            progress.setCurrentExercise(progress.getCurrentExercise()  + 1);
        }
    }

    public static void previousExercise(ExerciseCollection collection){
        ExerciseProgress progress = collection.getProgress();
        int exerciseTotal = progress.getFinishRecords().length;
        if (progress.getCurrentExercise() > 0 && exerciseTotal > 1) {

            progress.setCurrentSet(0);
            progress.setCurrentExercise(progress.getCurrentExercise()  - 1);
        }
    }

    public static int getPreviousViewVisibility(ExerciseCollection collection) {

        ExerciseProgress progress = collection.getProgress();
        if (progress.getCurrentExercise() == 0) {
            return View.INVISIBLE;
        }else{
            return View.VISIBLE;
        }
    }

    public static int getNextViewVisibility(ExerciseCollection collection) {

        ExerciseProgress progress = collection.getProgress();
        int exerciseTotal = progress.getFinishRecords().length;
        if (progress.getCurrentExercise() == exerciseTotal -1) {
            return View.INVISIBLE;
        }else{
            return View.VISIBLE;
        }
    }

    public static int getDoneViewVisibility(ExerciseCollection collection) {
        ExerciseProgress progress = collection.getProgress();
        if (progress.getFinishRecords()[progress.getCurrentExercise()]) {
            return View.INVISIBLE;
        } else {
            return View.VISIBLE;
        }
    }

    public static void quitRoutine(ExerciseCollection collection) {
        collection.getProgress().setCurrentGoup(-1);
    }
}
